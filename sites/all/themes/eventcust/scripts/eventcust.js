/**
 * .
 */
(function($) {
// поднимает курсор выше чем объект указаный в якоре ссылки, для учета высоты формы поиска
    //todo
    var y = 0;
    $(document).ready(function(){
                var need_moving = window.location.href.match('#')?true:false;
                if (need_moving) {
                    var target = window.location.href.split('#')[1];
                    if (target == 'block-user-recommendation-user-recommendation-info'){
                        $('.tabs-div').find('.active').removeClass('active');
                        $('.tab-content-active').removeClass('tab-content-active');
                        var id_tab = 'block-user-recommendation-user-recommendation-info';
                        $('#block-user-recommendation-user-recommendation-info').addClass('active');
                        $("div#" + id_tab + ' .content').addClass('tab-content-active');
                    }
                    target =   document.getElementById(target);
                    y = getOffsetRect(target).top - 330;
                }
    });
    window.onload=function() {
        if (y!=0) window.scrollTo(0,y);
            marginMenuTop(); //initialized movoving for search panel
    };
    //$(document).ready(function() {
    Drupal.behaviors.login_block = {
        attach: function (context, settings) {
            // Здесь указываем ID блока с логином
            var login_block = $('#block-user-login');
            if (login_block.length > 0) {

                // Далее при загрузке страницы
                // блок переместится в попап, но не покажется
                login_block.dialog({
                    autoOpen: false,
                    title: Drupal.t('Login to EventCust'),
                    resizable: false,
                    modal: true,
                    position: [screen.width - (screen.width-1000)/2 - 330,70]
                });

                // По клику на ссылку логина - показываем попап
                $('.login-button a').click(function() {
                    login_block.dialog('open');
					return false;
                });
				
				// По клику вне формы логина - закрываем попап
				$(document).click(function() {
                    login_block.dialog('close');
                });
				
				//Запрещаем скрывать попап по клику этих элементов
				$(".login-button a, .ui-draggable").click(function(e){
					e.stopPropagation();
				});
            }
        }
    }

    Drupal.behaviors.slider_chg = {
        attach: function (context, settings) {
            var sl = $("#block-slider-range");
            if (sl.length > 0) {
                if (Drupal.settings.slider_values != null){
                    var min_price_value = Drupal.settings.slider_values['min'];
                    var max_price_value = Drupal.settings.slider_values['max'];
                }
                else{
                    if (Drupal.settings.slider_settings != null) {
                        var min_price_value = Drupal.settings.slider_settings['min'];
                        var max_price_value = Drupal.settings.slider_settings['max'];
                    }

                }
                sl.slider({
                    range: true,
                    /*min: Math.min.apply(null,Drupal.settings.prices),
                    max: Math.max.apply(null,Drupal.settings.prices),*/
                    min: Drupal.settings.slider_settings['min'],
                    max: Drupal.settings.slider_settings['max'],
                    step: 100,
                    values: [min_price_value, max_price_value],
                    slide: function(event, ui){
                        $("#edit-block-min-price").val(ui.values[0]);
                        $("#edit-block-max-price").val(ui.values[1]);
                    }
                });
                $("#edit-block-min-price").val($("#block-slider-range").slider("values", 0));
                $("#edit-block-max-price").val($("#block-slider-range").slider("values", 1));

            }
        }
    }
    Drupal.behaviors.publish_slider_chg = {
        attach: function (context, settings) {
            var sl = $(".publish-link-block:not(.publish-slider-processed)");
            $.each(sl, function(key, value) {
                var sl_current = $(this);
                $(this).addClass('publish-slider-processed');
                var nid = sl_current.attr('id');
                if (Drupal.settings['publish_slider_' + nid] !== undefined) {
                    var value = Drupal.settings['publish_slider_' + nid];
                }
                else {
                    var value = 0;
                }
                sl_current.slider({
                    min: 0,
                    max: 1,
                    step: 1,
                    value: value,
                    stop: function(event, ui){
                        if (ui.value == 0){
                            $.post('/node/' + nid + '/unpublish/ajax', function(data){
                                if (data.code > 0){
                                    $('#' + nid + '.publish-link-block').removeClass("publish-slider-published");
                                    $('#' + nid + '.publish-link-block').addClass("publish-slider-unpublished");
                                    $('#' + nid + '.publish-link-block .publish-slider-text').html(Drupal.t('Hidden'));
                                }
                                else {
                                    alert(data.error);
                                    $('.advert-manage-links #' + nid).slider("value", 1);
                                }
                            });
                        }
                        else{
                            $.post('/node/' + nid + '/publish/ajax', function(data){
                                if (data.code > 0){
                                    $('#' + nid + '.publish-link-block').removeClass("publish-slider-unpublished");
                                    $('#' + nid + '.publish-link-block').addClass("publish-slider-published");
                                    $('#' + nid + '.publish-link-block .publish-slider-text').html(Drupal.t('Active'));
                                }
                                else {
                                    alert(data.error);
                                    $('.advert-manage-links #' + nid).slider("value", 0);
                                }
                            });
                        }
                    }
                });
                $(".advert-manage-links a.publish-slider-link-nojs").hide();
            });

        }
    }

    Drupal.behaviors.datepicker_chg = {
        attach: function (context, settings) {
            var dps = $("#edit-date-start-datepicker-popup-0");
            var dpe = $("#edit-date-end-datepicker-popup-0");
            if (dps.length > 0) {
                $('.deal-price-wrapper').hide();
                $("#edit-date-start-datepicker-popup-0").change(function(){
                        dpe.val(dps.val());
                        $('.form-item-date-end-date label').css('display', 'none');
                })
                $('#block-users-deal-conclude-contract input').change(price_recalc);
                $('.form-item-price-types #edit-price-types').change(price_recalc);

            }
        }
    }

    function price_recalc(){
        var dps = $("#edit-date-start-datepicker-popup-0");
        var dpe = $("#edit-date-end-datepicker-popup-0");
        //get work price
        var prices = Drupal.settings['prices'];
        var price_type =  $('.form-item-price-types #edit-price-types').find(':selected').val();
        //var day_price = $('.form-item-prices #edit-price-types').find(':selected').text();
        var day_price = prices[price_type];
        //change price block
        $('#block-ec-advert-advert-price .price-text').html(day_price);

        //create date objects from date textfields
        var date_start = dps.val();
        var date_start_day = date_start.substr(0,2);
        var date_start_month = date_start.substr(3,2);
        var date_start_year = '20' + date_start.substr(6,4);
        //var date_start_d = new Date(date_start_month +"." +date_start_day + "." + date_start_year);
        var date_start_d = new Date(date_start_year, date_start_month-1, date_start_day);

        var date_end = dpe.val();
        var date_end_day = date_end.substr(0,2);
        var date_end_month = date_end.substr(3,2);
        var date_end_year = '20' + date_end.substr(6,4);
        //var date_end_d = new Date(date_end_month +"." +date_end_day + "." + date_end_year);
        var date_end_d = new Date(date_end_year, date_end_month-1, date_end_day);

        //get days count
        var count = Math.ceil((date_end_d.getTime() - date_start_d.getTime() + 1)/60/60/24/1000);
        if (count>0 && date_end != '' && date_start != ''){
            $('.deal-price-wrapper').show();
            $('input[name=\'days_count\']').val(count);
            var all_price = day_price * count
            $('input[name=\'price_val\']').val(all_price);
            //format price - added thousand separator
            var num2 = all_price.toString().split('.');
            var thousands = num2[0].split('').reverse().join('').match(/.{1,3}/g).join(' ');
            var decimals = (num2[1]) ? ','+num2[1] : '';
            var all_price_formatted =  thousands.split('').reverse().join('')+decimals;
            //set price
            $('span.advert-price .all-deal-price').html(all_price_formatted);
            //clear error message
            $('#users-deal-create-deal-form .error-msg').html('');
        }
        else {
            $('.deal-price-wrapper').hide();
            $('input[name=\'days_count\']').val(1);
            $('input[name=\'price_val\']').val(day_price);
            //if isset date start and end and end > start
            if (date_end != '' && date_start != ''){
                $('#users-deal-create-deal-form .error-msg').html(Drupal.t('Error: Invalid dates'));
            }
        }

    }
    Drupal.behaviors.dropdown_click = {
        attach: function (context, settings) {
            var ddl = $("#block-ec-user-user-header-dropdown-menu #dropdown_link");
            if (ddl.length > 0) {
                hd = $('#block-ec-user-user-header-dropdown-menu .header-dropdown');
                hd.css('display', 'block');
                var v = hd.width();
                ddl.width(v);
                hd.css('display', 'none');
                ddl.removeClass('active');
                // По клику на ссылку меню - показываем попап
                ddl.click(function() {
                    if (ddl.hasClass('active')){
                        hd.hide();
                        ddl.removeClass('active');
                        hd.css('position', 'relative');
                    }
                    else{
                        hd.width(v);
                        hd.show();
                        ddl.addClass('active');
                        $(document).click(function(event) {
                            if ($(event.target).closest("#block-ec-user-user-header-dropdown-menu").length) return;
                            hd.css('display', 'none');
                            ddl.removeClass('active');
                            hd.css('position', 'relative');
                            event.stopPropagation();
                            $(document).unbind('click');
                        });
                    }
                    return false;
                });
            }
        }
    }

    var m1 = 100;
    var m2 = 0; /* отступ, когда во время прокрутки шапка
     уже не видна */


    /* функция кроссбраузерного определения
     отступа от верха документа к текущей позиции
     скроллера прокрутки */
    function getScrollTop() {
        var scrOfY = 0;
        if( typeof( window.pageYOffset ) == "number" ) {
            //Netscape compliant
            scrOfY = window.pageYOffset;
        } else if( document.body
            && ( document.body.scrollLeft
            || document.body.scrollTop ) ) {
            //DOM compliant
            scrOfY = document.body.scrollTop;
        } else if( document.documentElement
            && ( document.documentElement.scrollLeft
            || document.documentElement.scrollTop ) ) {
            //IE6 Strict
            scrOfY = document.documentElement.scrollTop;
        }
        return scrOfY;
    }

    /* функция, которая устанавливает верхний отступ
     для «плавающего» фиксированного вертикального
     меню в зависимости от положения
     скроллера и видимости шапки */


    function marginMenuTop() {
        var top  = getScrollTop();
        var s = document.getElementById("edit-block-basic");
        var admin = document.getElementById("admin-menu");
        if (admin) var m1 = 120; /* высота шапки в пикселях */
        else var m1 = 90;
        var spacer = document.getElementById("search-apache-solr-block-spacer");
        /*Если ширина окна меньше ширины панелт поиска, то фиксированая позиция отменяется*/
        if (s){
        if (window.innerWidth < s.offsetWidth) {
            s.className = "container-inline form-wrapper";
        s.style.paddingBottom = '11px';
        }
        else{
            s.className = "container-inline form-wrapper search-form-moved";
            s.style.paddingBottom = '1px';
        }
        if (top+m2 < m1) {
            s.className = "container-inline form-wrapper";
            s.style.paddingBottom = '11px';
            spacer.style.height = '0px';



        } else {
            s.style.top = m2 + "px";
            s.style.paddingBottom = '1px';
            spacer.style.height = '80px';
            //s.style.position = "fixed";
            //s.css("z-index", "9999");
        }
    }
    }
    /** функция регистрирует
     вычисление позиции
     «плавающего» меню при прокрутке страницы
     **/

    function setMenuPosition(){

        if(typeof window.addEventListener != "undefined"){
            window.addEventListener("scroll", marginMenuTop, false);

        } else if(typeof window.attachEvent != "undefined"){
            window. attachEvent("onscroll", marginMenuTop);
        }
    }

/** todo uncomment for fixed search-nlock on top of page**/
    if(typeof window.addEventListener != "undefined"){
        window.addEventListener("load", setMenuPosition, false);

    } else if(typeof window.attachEvent != "undefined"){
        window. attachEvent("onload", setMenuPosition);
    }
    // function for get left & top of element
    function getOffsetRect(elem) {
        var box = elem.getBoundingClientRect();
        var body = document.body;
        var docElem = document.documentElement;
        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
        var clientTop = docElem.clientTop || body.clientTop || 0;
        var clientLeft = docElem.clientLeft || body.clientLeft || 0;
        var top  = box.top +  scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;
        return { top: Math.round(top), left: Math.round(left) }
    }
})(jQuery);