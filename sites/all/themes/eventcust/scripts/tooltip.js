//todo
(function($) {
    Drupal.behaviors.reviewslink = {
        attach: function (context, settings) {
            var IE = document.all?true:false;
            var tooltip = document.createElement("div"); // tooltip
            var oInterval; // timer for remove tooltip when mouse out
            var basta = false; // if this boolean is true then script do not remove tooltip
            var target = ''; // chek target when ajx return data. If target has been changed then tooltip dont show
            $('#tooltip').live('mouseover', function () {
                basta = true;
            });

            //remove tooltip and interval (timer)
            function clearedinterval(basta_local){
                var chek = document.getElementById("tooltip")?true:false;
                if (chek  && !basta_local){
                    tooltip.innerHTML = 'Loading...';
                    document.body.removeChild(document.getElementById("tooltip"));
                    window.clearInterval(oInterval);
                }
            }
            // function for get left & top of element
            function getOffsetRect(elem) {
                var box = elem.getBoundingClientRect();
                var body = document.body;
                var docElem = document.documentElement;
                var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                var clientTop = docElem.clientTop || body.clientTop || 0;
                var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                var top  = box.top +  scrollTop - clientTop;
                var left = box.left + scrollLeft - clientLeft;
                return { top: Math.round(top), left: Math.round(left) }
            }
            // create tooltip for reviews
            $('#block-system-main .reviews-link').live('mouseover', function (e) {
                var chek = document.getElementById("tooltip")?true:false;
                var nid = new Array();
                var object =  e.target.parentNode; // for fix error when used count reviews on image
                var parent_node_actioning_element = null; // parent of starting action elementactioning_element = e.target;
                if (e.target.id) if( $('#'+e.target.id).hasClass('reviews-link')){
                    var object = e.target;
                }
                $('#'+object.id).removeClass('reviews-link').addClass('reviews-link-processed');
                target = object; // save target for all function
                nid = String(object).split('/');
                //if tooltip isset then removie it
                if (chek){
                    document.body.removeChild(document.getElementById("tooltip"));
                    basta = true; // запрещает удаление тултипа таймером т.к. мы его удалили досрочно.
                }
                //crate new tooltip for  this action
                document.body.appendChild(tooltip);
                tooltip.id = "tooltip";
                tooltip.style.left = getOffsetRect(e.target).left-200 + "px";
                tooltip.style.top = -300 + "px"; // hide tolltip
                $.get('/advert-reviews-tooltip/'+nid[5], {}, function (data) {
                    //look if  current target != current action object then skip function and removie class processed
                    if (target == object){
                        tooltip.className = '';
                        tooltip.innerHTML = data;
                        tooltip.style.top = getOffsetRect(e.target).top - tooltip.offsetHeight -20 + "px";
                    }
                        $('#'+object.id).removeClass('reviews-link-processed').addClass('reviews-link');


                });
            });

            // create tooltip for recommendation
            $('#block-system-main .recommendation-button-link').live('mouseover', function (e) {
                var chek = document.getElementById("tooltip")?true:false;
                var nid = new Array();
                var object =  e.target.parentNode;
                if(e.target.className == 'recommendation-button-link'){
                    var object = e.target;
                }
                $('#'+object.id).removeClass('recommendation-button-link').addClass('recommendation-button-link-processed');
                target = object; // save target for all function
                nid = String(object).split('/');
                if (chek){
                    document.body.removeChild(document.getElementById("tooltip"));
                    basta = true;
                }
                document.body.appendChild(tooltip);
                tooltip.id = "tooltip";
                tooltip.style.left = getOffsetRect(e.target).left-200 + "px";
                tooltip.style.top = -300 + "px";
                $.get('/advert-recommendation-tooltip/'+nid[5]+'/'+nid[6], function (data) {
                    if (target == object){
                        tooltip.className = '';
                        tooltip.innerHTML = data;
                        tooltip.style.top = getOffsetRect(e.target).top - tooltip.offsetHeight -20 + "px";
                    }
                        $('#'+object.id).removeClass('recommendation-button-link-processed').addClass('recommendation-button-link');

                });
            });
            // create tooltip for calendar
            /*
             $('#block-system-main .calendar-link').live('mouseover', function (e) {
             var chek = document.getElementById("tooltip")?true:false;
             var uid = new Array();
             var object =  e.target.parentNode;
             if (e.target.id) if( $('#'+e.target.id).hasClass('calendar-link')){
             var object = e.target;
             }
             $('#'+object.id).removeClass('calendar-link').addClass('calendar-link-processed');
             target = object; // save target for all function
             //if tooltip isset then remove and create new tooltip for this object
             if (chek){
             document.body.removeChild(document.getElementById("tooltip"));
             basta = true;
             }
             $('#'+object.id).addClass('processed');
             uid = String(object).split('/');
             document.body.appendChild(tooltip);
             tooltip.id = "tooltip";
             tooltip.style.left = getOffsetRect(e.target).left-100 + "px";
             tooltip.style.top = -300 + "px";
             $.get('/advert-calendar-tooltip-days-off/'+uid[5], {}, function (data) {
             if (target == object){
             if(data){
             Drupal.settings.days_off = data;
             }
             console.log('working');
             }
             else{
             $('#'+object.id).removeClass('calendar-link-processed').addClass('calendar-link');
             }
             $.get('/advert-calendar-tooltip/'+uid[5], {}, function (data) {
             if (target == object){
             tooltip.innerHTML = data;
             tooltip.className = 'tolltip-calndar';
             create_calendar(uid[5]);
             tooltip.style.top = getOffsetRect(e.target).top - 255 + "px";
             $('#'+object.id).removeClass('calendar-link-processed').addClass('calendar-link');
             console.log(data);
             console.log(tooltip.style.top);
             }
             else{
             $('#'+object.id).removeClass('calendar-link-processed').addClass('calendar-link');
             }
             });
             });
             }); */
            $('#block-system-main .calendar-link').live('mouseover', function (e) {
                var chek = document.getElementById("tooltip")?true:false;
                var uid = new Array();
                var object =  e.target.parentNode;
                if (e.target.id) if( $('#'+e.target.id).hasClass('calendar-link')){
                    var object = e.target;
                }
           //     $('#'+object.id).removeClass('calendar-link').addClass('calendar-link-processed');
                target = object; // save target for all function
                uid = String(object).split('/');
                if (chek){
                    document.body.removeChild(document.getElementById("tooltip"));
                    basta = true;
                }
                document.body.appendChild(tooltip);
                tooltip.id = "tooltip";
                tooltip.style.left = getOffsetRect(e.target).left-100 + "px";
                tooltip.style.top = -300 + "px";
                $.get('/advert-calendar-tooltip-days-off/'+uid[5], {}, function (data) {
                    Drupal.settings.days_off = data;
                });
                $.get('/advert-calendar-tooltip/'+uid[5], {}, function (data) {
                    if (target == object){
                        tooltip.innerHTML = data;
                        tooltip.className = 'tooltip-calendar';
                        create_calendar(uid[5]);
                        tooltip.style.top = getOffsetRect(e.target).top - 255 + "px";
                    }
                 //   $('#'+object.id).removeClass('calendar-link-processed').addClass('calendar-link');
                });
            });

            // useed for leave cursor actions
            function nulling_tooltip() {
                target = '';
                oInterval = window.setTimeout(function(){clearedinterval(basta)}, 1000);
                if (basta){
                    basta = false;
                }
            }

            $('#block-system-main .reviews-link').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#block-system-main .calendar-link').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#block-system-main .recommendation-button-link').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#block-system-main .reviews-link-processed').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#block-system-main .calendar-link-processed').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#block-system-main .recommendation-button-link-processed').live('mouseout', function () {
                nulling_tooltip();
            });
            $('#tooltip').live('mouseout', function () {
                target = '';
                oInterval = window.setTimeout(function(){clearedinterval(basta)}, 1000);
                if (basta){
                    basta = false;
                }
            });
            $('#block-system-main .calendar-link').live('click', function () {
                return false;
            });
            $('#block-system-main .reviews-link').live('click', function () {
                return false;
            });
            $('#block-system-main .recommendation-button-link').live('click', function () {
                return false;
            });


            function create_calendar(uid){
                var startDay = new Date();
                var endDay = new Date();
                var dates = [];
                var dealId = '';
                var editDeal = '';
                // Small calendar on user and big calendar page.
                if($('#small-calendar', context).length){

                    $.get('/rest-days-for-small-calendar/'+uid, function(data){
                        if(!$.isEmptyObject(data)){
                            dates = data['days'];
                        }
                        else{
                            dates = '1971-01-01';
                        }
                        $('#small-calendar').datepicker({
                            flat: true,
                            date: dates,
                            firstDay: 1,
                            minDate: 0,
                            format: 'Y-m-d',
                            calendars: 1,
                            mode: 'multiple',
                            onRender: function(date) {
                                return {
                                    disabled: (date.valueOf())
                                }
                            },
                            starts: 1,
                            //look every date in small calendar for make days_off
                            //marking only if  days_off oldest then now date
                            beforeShowDay: function(date) {

                                //форматируем обрабатываемую дату
                                var date_format = date.getFullYear()+'-';
                                if (date.getMonth()+1<10) date_format += '0' +(date.getMonth()+1)+'-';
                                else date_format += (date.getMonth()+1)+'-';
                                if (date.getDate()<10) date_format +='0'+date.getDate();
                                else date_format +=date.getDate();


                                var now = new Date();
                                var now_date_format = now.getFullYear()+'-';
                                if (now.getMonth()+1<10) now_date_format += '0'+(now.getMonth()+1)+'-';
                                else now_date_format += (now.getMonth()+1)+'-';
                                if (now.getDate()<10) now_date_format +='0'+now.getDate();
                                else now_date_format +=now.getDate();

                                var days_off = Drupal.settings.days_off;
                                if (days_off) {
                                    if (now_date_format <= date_format){
                                        if ($.inArray(date_format,Drupal.settings.days_off) >=0)
                                        {
                                            return [false,"day-off"];
                                        }
                                    }
                                }
                                return [true, ""];//enable all other days
                            }
                        });

                    });
                };


            }
        }
    }
})(jQuery);