<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

function eventcust_search_page($results){
  //dsm($results);
}

function eventcust_apachesolr_search_page_alter(&$build, $search_page){
  //dsm($build['search_results']['#results']);

}

function eventcust_theme() {
  return array(
    'apachesolr_custom_search_category_item' => array(
      'variables' => array('search' =>null, 'term' => null),
    ),
    'apachesolr_custom_advert_block_item' => array(
      'variables' => array('item' => null),
    ),
  );
}

function eventcust_apachesolr_query_alter($query) {
    $query->addParam('fl','im_field_work_type');
}

/*
 * Theme function for advert item in block
 */
function eventcust_apachesolr_custom_advert_block_item($vars) {
  $output  = '<div class="block-advert-title">' . l($vars['title'], $vars['link']) . '</div>';
  $output .= '<div class="block-advert-foto">' . l(theme_image(array('path'=>image_style_url('advert_block_img', $vars['fields']['tm_field_ads_fotos'][0]), 'attributes'=>array())), $vars['link'], array('html' => TRUE)) . '</div>';
  $output .= '<div class="block-advert-price">' . $vars['fields']['fss_advert_price'] . t('RUB') . '</div>';
  return $output;
}

/*
 * Theme function for search item
 * Used in columns view of search results and homepage panel
 */
function eventcust_apachesolr_custom_search_category_item($vars) {
  $term = $vars['term'];
  $full_term = taxonomy_term_load($term->tid);
  $language = i18n_language_interface();
  $search = $vars['search'];
  $category_ads_count = $vars['category_ads_count'];

  $search_options = $search['options'];
  $search_options['query'] += drupal_get_query_parameters();
  $search_options += array('absolute' => TRUE, 'html' => TRUE, 'query' => drupal_get_query_parameters());
  $output = '<div id = "category-' . $term->tid . '" class="node clearfix solr-search-results-category-item">';
  $output .= '<span class="name">'.  l(i18n_taxonomy_term_name($term, $language->language), $search['path'], $search_options) . '</span>';

  /*if (isset( $item['fields']['tm_field_ads_fotos'][0])) {
    $output .= '<span class="img">' . l(theme_image(array('path'=>image_style_url('solr_search_frontpage_category_img', $item['fields']['tm_field_ads_fotos'][0]), 'attributes'=>array())), $search['path'], $search_options) . '</span>';
  }*/

  //dsm(taxonomy_term_load($term->tid));
  if (isset($full_term->field_category_foto['und'][0]['uri'])) {
    $output .= '<span class="img">' . l(theme_image(array('path'=>image_style_url('solr_search_frontpage_category_img', $full_term->field_category_foto['und'][0]['uri']), 'attributes'=>array())), $search['path'], $search_options) . '</span>';
  }
    $output .=   l('<span class="count-adv">'.$category_ads_count . t(' adverts'). '</span>',$search['path'], $search_options);
    $output .= '</div>';

  return $output;
  
}

function eventcust_links__locale_block($vars){
  global $language;
  $params = drupal_get_query_parameters();
  $vars['links']['en']['title'] = substr($vars['links']['en']['title'], 0, strpos($vars['links']['en']['title'], '/>')+2);
  $vars['links']['en']['query'] = $params;
  $vars['links']['ru']['title'] = substr($vars['links']['ru']['title'], 0, strpos($vars['links']['ru']['title'], '/>')+2);
  $vars['links']['ru']['query'] = $params;
  $http_pos=strpos($vars['links']['en']['title'], "http");
  $vars['links']['en']['title'] = substr_replace($vars['links']['en']['title'], base_path() . drupal_get_path('theme', 'eventcust').'/images/en.png', $http_pos, strpos($vars['links']['en']['title'], '"', $http_pos)-$http_pos);
  $http_pos=strpos($vars['links']['ru']['title'], "http");
  $vars['links']['ru']['title'] = substr_replace($vars['links']['ru']['title'], base_path() . drupal_get_path('theme', 'eventcust').'/images/ru.png', $http_pos, strpos($vars['links']['ru']['title'], '"', $http_pos)-$http_pos);

  unset($vars['links'][$language->language]);
  return theme('links', $vars);
}

function eventcust_menu_link__menu_header_register_menu($vars) {
  $element = $vars['element'];
  if ($element['#href'] == 'user/login'){
    $element['#attributes']['class'][] = 'login-button';
  }
  $sub_menu = '';

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function eventcust_preprocess_page(&$vars) {
  global $user;
  if (isset($vars['node']) && $vars['node']->type == 'advert') $vars['tabs'] = FALSE;
  // Подключаем библиотеку только для анонимов
  if (!$user->uid) {
    drupal_add_library('system', 'ui.dialog');
    // Добавляем ссылку, при нажатии на которую будет показываться логин
    //$vars['login_button'] = l(t('Login'), 'user', array('attributes' => array('class' => array('user-login'))));
  }
}

function eventcust_preprocess_search_results(&$variables) {
  // Initialize variables
  $env_id = NULL;
  // If this is a solr search, expose more data to themes to play with.
  if ($variables['module'] == 'apachesolr_search') {
    // Fetch our current query
    if (!empty($variables['search_page']['env_id'])) {
      $env_id = $variables['search_page']['env_id'];
    }
    $query = apachesolr_current_query($env_id);

    if ($query) {
      $variables['query'] = $query;
      $variables['response'] = apachesolr_static_response_cache($query->getSearcher());
    }

    $filters = $query->getFilters();

    $category_view = true;
    foreach ($filters as $filter){
      if ($filter['#name'] == 'im_field_work_type') {
        $category_view = false;
        break;
      }
    }

    // Add template hints for environments
    if (!empty($env_id)) {
      if ($category_view){
        unset($variables['pager']);
        $variables['search_results'] = _apachesolr_custom_get_category_result();
      }
      else{
        if (empty($variables['response'])) {
          $variables['description'] = '';
          return;
        }
        $total = $variables['response']->response->numFound;
        $params = $variables['query']->getParams();

        $variables['description'] = t('Showing items @start through @end of @total.', array(
          '@start' => $params['start'] + 1,
          '@end' => $params['start'] + $params['rows'] - 1,
          '@total' => $total,
        ));
        // Redefine the pager if it was missing
        pager_default_initialize($total, $params['rows']);
        $variables['pager'] = theme('pager', array('tags' => NULL));

        $variables['theme_hook_suggestions'][] = 'search_results__' . $variables['module'] . '__' . $env_id;
        // Add template hints for search pages
        if (!empty($variables['search_page']['page_id'])) {
          $variables['theme_hook_suggestions'][] = 'search_results__' . $variables['module'] . '__' . $variables['search_page']['page_id'];
          // Add template hints for both
          $variables['theme_hook_suggestions'][] = 'search_results__' . $variables['module'] . '__' . $env_id . '__' . $variables['search_page']['page_id'];
        }
      }
    }
  }
}

/**
 * Process variables for search-result.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $result
 * - $module
 *
 * @see search-result.tpl.php
 */
function eventcust_preprocess_search_result(&$variables) {
  global $language;
  $path = drupal_get_path('module', 'calendar');
  drupal_add_css($path . '/css/calendar.css');
  drupal_add_js(drupal_get_path('theme', 'eventcust') . '/scripts/tooltip.js');
  $params = drupal_get_query_parameters();
  $result = $variables['result'];
  $node = $result['node'];
    if (isset($params['date_start'])) {
      if (isset($params['date_end'])){
        $variables['url_query'] = '1';
        $variables['date_start']=$params['date_start'];
        $variables['date_end']=$params['date_end'];
      }}
  $variables['title'] = check_plain($result['title']);
  if (isset($result['language']) && $result['language'] != $language->language && $result['language'] != LANGUAGE_NONE) {
    $variables['title_attributes_array']['xml:lang'] = $result['language'];
    $variables['content_attributes_array']['xml:lang'] = $result['language'];
  }

    if ($variables['module'] == 'ec_advert'){
    $destination = drupal_get_destination();
  //  $variables['manage_link'] = l(t('Manage advert'), 'node/' . $node->nid . '/edit', array('query'=>$destination));
      $variables['manage_link'] = l('<span class="manage-advert-img"></span><span class="manage-advert-text">'.t('Manage advert').'</span>', 'node/' . $node->nid . '/edit', array('attributes'=>array('class' => array('manage-advert-link')), 'html' => true,'query'=>$destination));
    if ($node->status){
      $publish_link_text = t('Unpublish Advert');
      $publish_link_action = 'unpublish';
      $publish_slider_status = 'publish-slider-published';
      $publish_slider_text = t('Active');
    }
    else{
      $publish_link_text = t('Publish Advert');
      $publish_link_action = 'publish';
      $publish_slider_status = 'publish-slider-unpublished';
      $publish_slider_text = t('Hidden');
    }
    drupal_add_js(array('publish_slider_' . $node->nid => $node->status), 'setting');
    $variables['publish_link'] = '<div id="' .$node->nid . '" class="publish-link-block ' . $publish_slider_status . '"><div class="publish-slider-text">' . $publish_slider_text . '</div></div>'
      . l($publish_link_text, 'node/' . $node->nid . '/' . $publish_link_action, array('attributes'=> array('class' => 'publish-slider-link-nojs')));
    drupal_add_library('system', 'ui.slider');

    if (isset($node->field_short_description['und'][0]['value'])){
    $body = $node->field_short_description['und'][0]['value'];
    }
    $field_ads_fotos = isset($node->field_ads_fotos['und'][0]['uri']) ? $node->field_ads_fotos['und'][0]['uri'] : '';
    $field_work_price = ec_advert_get_default_price($node->field_work_type['und'][0]['tid'], $node->nid);
      $variables['calendar_link'] = l('<span class="calendar-img"></span>' . t('<span class="calendar-text">'.t('calendar').'</span>'), 'user/' . $node->uid . '/ads/calendar', array('attributes'=>array('id' =>'calendar-link-'.$node->nid,'class' => array('calendar-link')), 'html' => true));
      //get recommendation count and recommendation button
      $query = user_recommendation_get_recommendation($node->uid);
      $rec_count = $query->rowCount();
      if ($rec_count >0) {
        $variables['rec_link'] = l($rec_count.' '.t('recommendations'), 'node/'. $node->nid .'/'. $node->uid, array('attributes'=>array('id' =>'recommendation-button-link-'.$node->nid,'class' => array('recommendation-button-link'))));
      }
    //get reviews count
    $query = ec_advert_get_reviews($node->nid,0);
    $review_count = $query->execute()->rowCount();
    if ($review_count >0) {
     $variables['reviews_link'] = l($review_count.' '.t('reviews'), 'node/'. $node->nid .'/'. $node->uid, array('attributes'=>array('id' =>'reviews-link-'.$node->nid,'class' => array('reviews-link'))));
    }
  }
  else{
    $variables['url'] = 'node/'.$result['node']->entity_id;
    $body = $node->ts_short_description;
    $field_ads_fotos = isset($node->tm_field_ads_fotos[0]) ? $node->tm_field_ads_fotos[0] : '';
    $field_work_price = $result['node']->fss_advert_price ;
    $variables['calendar_link'] = l('<span class="calendar-img"></span>' . t('<span class="calendar-text">'.t('calendar').'</span>'), 'user/' . $node->uid . '/ads/calendar', array('attributes'=>array('id' =>'calendar-link-'.$node->entity_id,'class' => array('calendar-link')), 'html' => true));
    //get recommendation count and recommendation button
    $query = user_recommendation_get_recommendation($node->uid);
    $rec_count = $query->rowCount();
    if ($rec_count >0) {
      $variables['rec_link'] = l($rec_count.' '.t('recommendations'), 'node/'. $node->entity_id .'/'. $node->uid, array('attributes'=>array('id' =>'recommendation-button-link-'.$node->entity_id,'class' => array('recommendation-button-link'))));
    }
    //get reviews count
    $query = ec_advert_get_reviews($node->entity_id,0);
    $review_count = $query->execute()->rowCount();
    if ($review_count >0) {
    //$variables['reviews_link'] = '<div class="reviews-zone">'.l('<div class="review-ico">'.''.$review_count.' '.'</div><div class="review-link-text">'.t('reviews').'</div>', 'user/' . $node->uid.'/'. $node->entity_id, array('attributes'=>array('class' => array('reviews-link')),'html' => TRUE)).'</div>';
      $variables['reviews_link'] = l($review_count.' '.t('reviews'), 'node/'. $node->entity_id .'/'. $node->uid, array('attributes'=>array('id' =>'reviews-link-'.$node->entity_id,'class' => array('reviews-link'))));
    }
    }

  $ads_number = '<div class="advert-number">' . $variables['id'] . '</div>';
  if (isset($params['date_start']) && isset($params['date_end'])){
  $variables['advert_foto'] = l(theme_image(array('path'=>image_style_url('search_result_img', $field_ads_fotos), 'attributes'=>array())) . $ads_number, $variables['url'], array('html' => TRUE, 'query' => array('date_start' => $params['date_start'], 'date_end' => $params['date_end'])));
  }
  else
  {
    $variables['advert_foto'] = l(theme_image(array('path'=>image_style_url('search_result_img', $field_ads_fotos), 'attributes'=>array())) . $ads_number, $variables['url'], array('html' => TRUE,));
  }
  $author = user_load($node->uid);
  $filepath = ec_user_get_avatar_url($author->uid);
  $variables['author_foto'] = '<div class = "search-item-author-avatar">' . l(theme('image_style', array('style_name' => 'search_result_author_avatar', 'path' => $filepath, 'alt' => $author->name, 'title' => $author->name)), 'user/' . $author->uid, array('html' => TRUE)) . '</div>';

  if (isset($body)) $variables['description'] = $body;
  else $variables['description'] = '';
  $variables['price'] = '<div class="price-text">' . $field_work_price . '</div>';
  $variables['price'] .= theme('image', array('path' => base_path() . drupal_get_path('theme', 'eventcust').'/images/ruble.png', 'attributes' => array('class' => array('img-ruble'))));

  //todo change rating
  $variables['rating'] = '<span class="rating_g"></span><span class="rating_g"></span><span class="rating_g"></span><span></span><span></span>';
  $variables['theme_hook_suggestions'][] = 'search_result__' . $variables['module'];
}

function eventcust_apachesolr_sort_link($vars) {
  $icon = '';
  if ($vars['direction']) {
    $icon = '<span class="search-sort-indicator ' . $vars['direction'] . '"></span>';
  }
  if ($vars['active']) {
    if (isset($vars['options']['attributes']['class'])) {
      $vars['options']['attributes']['class'] .= ' active';
    }
    else {
      $vars['options']['attributes']['class'] = 'active';
    }
  }
  return apachesolr_l($vars['text'], $vars['path'], $vars['options']) .$icon;
}


function  eventcust_form_user_login_block_alter(&$form, &$form_state)
{
	$form['name']['#size'] = 29;
	$form['pass']['#size'] = 29;

	$form['links']['#markup'] = l(t('Request new password'), 'user/password', array('attributes' => array('title' => t('Request new password via e-mail.'))));
	$form['links']['#weight'] = 5;
	$form['fbconnect_button']['#weight'] = -3;
}

function eventcust_field__field_description__advert($variables){
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  $output .= '<div class="advert-field-separator-wrapper"><div class="advert-field-separator"></div></div>';
  return $output;
}

function eventcust_field__field_short_description__advert($variables){
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  $output .= '<div class="advert-field-separator-wrapper"><div class="advert-field-separator"></div></div>';
  return $output;
}

function eventcust_username($account){

    $current_user = $account['account'];
    global $user;

    $first_name = isset($current_user->field_first_name) ? $current_user->field_first_name['und']['0']['value'] : t('Anonymus');
    $last_name = isset($current_user->field_last_name) ? $current_user->field_last_name['und']['0']['value'] : '';
    $admin = user_access('administer site configuration');
    $have_deal = is_users_have_deal($current_user, $user);
    if(!$admin || !$have_deal || $current_user->uid!=$user->uid){
        $last_name = mb_substr($last_name, 0, 1) . '.';
    }
    $name = $first_name . ' ' . $last_name;
    return $name;
}
