<?php

/**
 * @file
 * Provide mobile commons integration for SMS framework.
 */

/**
 * Implements hook_gateway_info().
 */
function sms_mobile_commons_gateway_info() {
  return array(
    'sms_mobile_commons' => array(
      'name' => 'SMS Mobile Commons',
      'send' => 'sms_mobile_commons_send',
      'receive' => TRUE,
      'configure form' => 'sms_mobile_commons_admin_form',
      'send form' => 'sms_mobile_commons_send_form',
    )
  );
}

/**
 * Implements hook_admin_form().
 */
function sms_mobile_commons_admin_form($configuration) {
  if (!is_null($configuration['sms_mobile_commons_password'])) {
    $password_needed = FALSE;
    $password_message = "A password HAS been set.";
  }
  else {
    $password_needed = TRUE;
    $password_message = "A password HAS NOT been set.";

  }
  $form = array();
  $form['sms_mobile_commons_custom_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile Commons custom URL'),
    '#default_value' => $configuration['sms_mobile_commons_custom_url'],
    '#description' => t('The custom URL used to access your Mobile Commons API. It often looks like "http://MYACCOUNT.mcommons.com".'),
    '#required' => TRUE,
  );
  $form['sms_mobile_commons_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#default_value' => $configuration['sms_mobile_commons_email'],
    '#description' => t('The email address that is used to authenticate you to Mobile Commons'),
    '#required' => TRUE,
  );
  $form['sms_mobile_commons_campaign'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign ID'),
    '#default_value' => $configuration['sms_mobile_commons_campaign'],
    '#description' => t('The campaign ID of your Mobile Commons campaign that the users are associted with.'),
  );
  $form['sms_mobile_commons_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => $configuration['sms_mobile_commons_password'],
    '#description' => t('The password to access Mobile Commons APIs. NOTE: ' . $password_message),
    '#required' => $password_needed,
  );
  $form['sms_mobile_commons_opt_in_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Opt In Path'),
    '#description' => t('The opt-in path on mobile commons. This is usually a number of about 5 digits.'),
    '#default_value' => $configuration['sms_mobile_commons_opt_in_path'],
  );
  $form['sms_mobile_commons_response'] = array(
    '#type' => 'textfield',
    '#title' => t('Response text for mData.'),
    '#description' => t('A default response to give back when the site receives a message via mData. In most cases you will want to ignore the use of a default response and instead implement hook_sms_mobile_commons_response_alter().'),
    '#default_value' => $configuration['sms_mobile_commons_response'],
  );

  return $form;

}

/**
 * Implements hook_form_alter().
 */
function sms_mobile_commons_form_sms_admin_gateway_form_alter(&$form, &$form_state) {
  $form['#validate'][] = 'sms_mobile_commons_admin_form_validate';
}

/**
 * Provides form validation for the SMS Mobile Commons gateway configuration.
 */
function sms_mobile_commons_admin_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['sms_mobile_commons_email'])) {
    form_set_error($name = 'sms_mobile_commons_email', $message = 'The email address was not valid');
  }
  if (!valid_url($form_state['values']['sms_mobile_commons_custom_url'])) {
    form_set_error($name = 'sms_mobile_commons_custom_url', $message = 'The url was not valid');
  }
}

/**
 * Custom callback for incoming HTTP request.
 */
function sms_mobile_commons_incoming_callback() {
  $message = $_REQUEST['args'];
  $keyword = $_REQUEST['keyword'];
  $carrier = $_REQUEST['carrier'];
  $sender = $_REQUEST['phone'];
  $gateway = sms_gateways('gateway', 'sms_mobile_commons');
  $config = $gateway['configuration'];
  $output = $config['sms_mobile_commons_response'];
  drupal_alter('sms_mobile_commons_response', $output, $message, $sender, $keyword);
  sms_incoming($sender, $message, array("carrier" => $carrier, 'keyword' => $keyword, 'response' => $output));
  watchdog('sms_mobile_commons', 'Received message from %sender: %message and responded with %response', array('%sender' => $sender, '%message' => $message, '%response' => $output));
  $output =
  '<?xml version="1.0" encoding="UTF-8"?>
  <response>
   <reply>
    <text>
     <![CDATA[' . $output . ']]>
    </text>
   </reply>
  </response>';
  print $output;
  return NULL;
}

/**
 * Implements hook_menu().
 */
function sms_mobile_commons_menu() {
  $items = array();

  $items['sms/mobile-commons/receiver'] = array(
    'title' => 'Mobile Commons SMS receiver',
    'page callback' => 'sms_mobile_commons_incoming_callback',
    'access callback' => 'sms_mobile_commons_incoming_check',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Add a filter to ensure incoming texts are coming from Mobile Commons' IP.
 */
function sms_mobile_commons_incoming_check() {
  // Valid IP range as specified by Mobile Commons is: 64.22.127.0/24.
  $valid = FALSE;
  $ip = $_SERVER['REMOTE_ADDR'];
  $valid = FALSE;
  if (strpos($ip, '64.22.127') === 0) {
    if ((int) substr($ip, 9, 3) >= 0 && (int) substr($ip, 10, 3) <= 24) {
      $valid = TRUE;
    }
  }
  return $valid;
}

/**
 * Implements hook_send().
 */
function sms_mobile_commons_send($number, $message, $options) {
  $status = FALSE;
  $return_message = "NULL";

  if (strlen($message) > 160) {
    $return_message = t('The message you wanted to send is too long and cannot be sent.');
    return array(
      'status' => $status,
      'message' => $return_message,
      'variables' => NULL,
    );
  }

  // Get configuration parameters
  $gateway = sms_gateways('gateways');
  $config = $gateway['sms_mobile_commons']['configuration'];
  $api_send_url = $config['sms_mobile_commons_custom_url'];
  $auth_string = $config['sms_mobile_commons_email'] . ':' . $config['sms_mobile_commons_password'];

  // Remove http or https at beginning of string.
  $api_send_url = str_replace('https://', '', $api_send_url);
  $api_send_url = str_replace('http://', '', $api_send_url);

  $api_check_user_url = 'https://' . $api_send_url . '/api/profile';
  $api_send_url = 'https://' . $api_send_url . '/api/send_message';

  // If option is set allow override of default campaign id.
  if (isset($options['campaign_id'])) {
    $campaign_id = $options['campaign_id'];
  }
  else {
    // Otherwise just use the default.
    $campaign_id = $config['sms_mobile_commons_campaign'];
  }

  // Find out if user has opted-out if they have don't send a message.
  $opts = array(
    'http' => array(
      'method'  => 'GET',
      'header'  => sprintf("Authorization: Basic %s\r\n", base64_encode($auth_string)),
    ),
  );
  $context = stream_context_create($opts);
  $return_val = file_get_contents($api_check_user_url . '?phone_number=' . $number, FALSE, $context);

  if (strpos($return_val, 'status="Opted-Out"') !== FALSE) {
    $return_message = t('User has opted out. Don\'t send spam.');
  }
  elseif (strpos($return_val, '<status>Undeliverable</status>') !== FALSE) {
    // Messages cannot be sent to this phone number.
    $return_message = t('Message cannot be delivered because the user has a bad phone number.');
  }
  else {
    // Send the message.
    $return_val = sms_mobile_commons_message_send($number, $message, $campaign_id, $auth_string);

    if (strpos($return_val, '<response success="true"/>') !== FALSE) {
      $status = TRUE;
      $return_message = t('The message was successfully sent.');
    }
    else {
      // Message failed. Might be a bad phone number. But mobile commons
      // returns 'Invalid phone number' for non phone numbers as well as
      // those not in the system.
      $return_message = t('Falied to send message');

      if ($options['auto_add'] === TRUE) {
        //Add users to campaign and try again.
        sms_mobile_commons_add_user_to_campaign($number, '', '', $campaign_id);
        $return_val = sms_mobile_commons_message_send($number, $message, $campaign_id, $auth_string);
        if (strpos($return_val, '<response success="true"/>') !== FALSE) {
          $status = TRUE;
          $return_message = t('After adding the user to the campaign, the message was successfully sent.');
        }
        else {
          $return_message = t('Failed to send the message second time.');
        }
      }
    }
  }

  $result = array(
    'status' => $status,
    'message' => $return_message,
    'variables' => NULL,
  );
  return $result;
}

/**
 * Adds a user to a campaign.
 *
 * @param $number
 *   The user's phone number.
 * @param $first_name
 *   The user's first name.
 * @param $last_name
 *   The user's last name.
 */
function sms_mobile_commons_add_user_to_campaign($number, $first_name = '', $last_name = '', $campaign_id = NULL) {
  //Get configuration parameters
  $gateway = sms_gateways('gateways');
  $config = $gateway['sms_mobile_commons']['configuration'];
  $opt_in_path = $config['sms_mobile_commons_opt_in_path'];
  $custom_url = $config['sms_mobile_commons_custom_url'];

  // Get the default campaign id if it has not been overridden.
  if (is_null($campaign_id)) {
    // Otherwise just use the default.
    $campaign_id = $config['sms_mobile_commons_campaign'];
  }

  // Remove http or https at beginning of string.
  $custom_url = str_replace('https://', '', $custom_url);
  $custom_url = str_replace('http://', '', $custom_url);

  $api_opt_in_path_url = 'https://' . $custom_url . '/campaigns/' . $campaign_id . '/subscriptions?opt_in_path=' . $opt_in_path;

  // Send the post data.
  $opts = array(
    'http' => array(
      'method'  => 'POST',
      'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
      'content' => http_build_query(array(
        'person[phone]' => $number,
      )),
    )
  );
  $context = stream_context_create($opts);
  return file_get_contents($api_opt_in_path_url, FALSE, $context);
}

/**
 * Send a message to an SMS number.
 *
 * @param $number
 *   The number the message should be sent to.
 * @param $message
 *   The message to send to the specified number.
 * @param $campaign_id
 *   The Mobile Commons ID to use.
 * @param $auth_string
 *   The Mobile Commons API key used for authorization.
 */
function sms_mobile_commons_message_send($number, $message, $campaign_id, $auth_string) {
  $opts = array(
    'http' => array(
      'method'  => 'POST',
      'header'  => sprintf("Authorization: Basic %s\r\n", base64_encode($auth_string)) . "Content-type: application/x-www-form-urlencoded\r\n",
      'content' => http_build_query(array(
        'campaign_id' => $campaign_id,
        'body' => $message,
        'phone_number' => $number,
      )),
    )
  );

  $context = stream_context_create($opts);
  return file_get_contents($api_send_url, FALSE, $context);
}
