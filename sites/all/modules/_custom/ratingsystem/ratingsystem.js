(function ($) {
  var sliders = [];
  var keyFlag = false;
  var currentPressedSlider = -1;

  Drupal.behaviors.exampleModule = {
    attach: function (context, settings) {
      $('#reset_button').click(function() {
        keyFlag = true;
        defaltClearAll();
      });
      sliders[0] = $('#picture_slider').slider({change:sliderChangedHandler});

      $('#picture_decrement_button').click(function() {
        keyFlag = true;
        decrementHandler('picture_decrement_button');
      });
      $('#picture_increment_button').click(function() {
        keyFlag = true;
        incrementHandler('picture_increment_button');
      });

      for(var i = 0; i < settings['users_fields'].length; i++) {
        sliders[settings['users_fields'][i][0]] = $('#' + settings['users_fields'][i][1] + '_slider').slider({change:sliderChangedHandler});
        $('#' + settings['users_fields'][i][1] + '_decrement_button').click(function() {
          keyFlag = true;
          decrementHandler(this.id);
        });
        $('#' + settings['users_fields'][i][1] + '_increment_button').click(function() {
          keyFlag = true;
          incrementHandler(this.id);
        });
      }
      defaltClearAllStores();
    }
  };

  function decrementHandler(decrButtId) {
    var decr_but_first_token = decrButtId.substring(0, decrButtId.lastIndexOf('_decrement_button'));
    var slider_s_first_token = '';
    var current_slider_s_id = '';
    //alert(sliders[0].val(sliders[0].attr("id")));
    for(var i in sliders) {
      current_slider_s_id = sliders[i].attr("id");
      slider_s_first_token = current_slider_s_id.substring(0, current_slider_s_id.lastIndexOf('_slider'));
      if(slider_s_first_token == decr_but_first_token) {
        if((sliders[i].slider('value') - 1) >= 0)
          sliders[i].slider('value', sliders[i].slider('value') - 1);
      }
    }
  }

  function incrementHandler(incrButtId) {
    var incr_but_first_token = incrButtId.substring(0, incrButtId.lastIndexOf('_increment_button'));
    var slider_s_first_token = '';
    var current_slider_s_id = '';
    //alert(sliders[0].val(sliders[0].attr("id")));
    for (var i in sliders) {
      current_slider_s_id = sliders[i].attr("id");
      slider_s_first_token = current_slider_s_id.substring(0, current_slider_s_id.lastIndexOf('_slider'));
      if (slider_s_first_token == incr_but_first_token) {
        if(getTotalValue() + 1 <= 100)
          sliders[i].slider('value', sliders[i].slider('value') + 1);
      }
    }
  }

  function addedVerify(comparedFirst, comparedSecond, m) {
    var max = m;
    if(comparedFirst > m) {
      max = comparedFirst;
    } else {
      if (comparedSecond > m) {
        max = comparedSecond;
      }
    }
    return max;
  }

  function getMaxDiff() {
    var Z = sliders.length - 2;
    var CZ = Z;
    var max = 0;
    var res = 0;
    var pres = 0;
    var firstSlideValue = 0;
    var SecondSliderValue = 0;
    var MaxName = -1;
    var minValue = 100;
    var resarr = [];
    var max_value = 0;

    for (var i in sliders) {
      if (sliders[i].slider('value') < minValue) {
        minValue = sliders[i].slider('value');
      }
    }

    //Set balance
    if (Z > 0) {
      for (var i = 0; i <= Z; i++) {
        if (CZ != 0) {
          for (var j = (sliders.length - 1) - CZ; j <= sliders.length - 1; j++) {
            firstSlideValue = sliders[i].slider('value');
            SecondSliderValue = sliders[j].slider('value');
            max_value = addedVerify(firstSlideValue, SecondSliderValue, max_value);
            res = firstSlideValue - SecondSliderValue;
            pres = Math.abs(res);
            //alert(pres);
            if (pres > max) {
              max = pres;
              if (res > 0) {
                MaxName = i;
              }
              else {
                MaxName = j;
              }
            }
            if (firstSlideValue == SecondSliderValue)
            {
              if((firstSlideValue > max_value) || (SecondSliderValue > max_value)) {
                MaxName = i;
              }
            }
          }
          CZ -= 1;
        }
        else {
          break;
        }
      }
    }
    else {
      firstSlideValue = sliders[0].slider('value');
      SecondSliderValue = sliders[1].slider('value');
      res = firstSlideValue - SecondSliderValue;
      pres = Math.abs(res);
      if (pres > max) {
        max = pres;
        if (res > 0) {
          MaxName = 0;
        }
        else {
          MaxName = 1;
        }
      }
      if(firstSlideValue == SecondSliderValue)
        MaxName = 0;
    }

    resarr[0] = max;
    resarr[1] = MaxName;
    resarr[2] = null;
    resarr[3] = minValue;

    return resarr;
    /////////////
  }

  function slidersCaushion(currentSlider) {
    var tot = 0;
    var r = getMaxDiff();
    var minElements = [];
    var decrementStep = 0;

    for (var i in sliders) {
      if (sliders[i].slider('value') == r[3])
        minElements.push(i);
    }
    decrementStep = minElements.length * 2;
    tot = getTotalValue();
    do {
      if (tot > 100) {
        sliders[r[1]].slider('value', (sliders[r[1]].slider('value')) - decrementStep);
        for (var k = 0; k < minElements.length; k++) {
          sliders[minElements[k]].slider('value', (sliders[minElements[k]].slider('value')) + 1);
        }
      }
      tot = getTotalValue();
    } while (tot > 100);
    if(tot < 100) {
      do {
        for(decrementStep in sliders) {
          if((sliders[decrementStep].attr('id') == currentPressedSlider)) {
            sliders[decrementStep].slider('value', (sliders[decrementStep].slider('value')) + 1);
          }
        }
        tot = getTotalValue();
      } while(tot != 100);
    }
  }

//This function is erased all data from stores
  function defaltClearAllStores() {
    var currName = '';
    for (var i in sliders) {
      currName = sliders[i].attr('id') + '_store';
      document.getElementById(currName).value = 0;
    }
    $('#coeff').attr('value', 0);
  }

//This function is erased all data from sliders
  function defaltClearAll() {
    var currName = '';
    for (var i in sliders) {
      sliders[i].slider('value', 0);
    }
  }

  function displayErrors() {
    if(getTotalValue() >= 100) {
      $('#sliders_error').show();
    } else {
      $('#sliders_error').hide();
    }
  }

  function sliderChangedHandler(event, ui) {
    // get value of current slider

    var currVal = $(this).slider('value');

    if (event.which !== undefined) {
      keyFlag = true;
    }
    if (keyFlag == true) {
      currentPressedSlider = this.id;
    }
    keyFlag = false;

    displayErrors();
    $('#coeff').attr('value', getTotalValue() / 10);
    $('#' + $(this).attr('id') + '_store').attr('value', $(this).slider('value'));
    // if the totalValue minus currVal + ui.value is greater than 100, return false
    var tot = 0;
    tot = getTotalValue() - currVal + ui.value;

    if (tot > 100) {
      slidersCaushion(this.id);
      return false;
    }



  }

// utility function to get the total value of sliders
  function getTotalValue() {
    var res = 0;
    for (var i in sliders) {
      res += sliders[i].slider('value');
    }
    return res;
  }


})(jQuery);