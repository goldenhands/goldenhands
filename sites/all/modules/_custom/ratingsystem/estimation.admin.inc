<?php
/**
 * @file
 * Administration page callbacks for the ratingsystem module.
 */

/**
 * Define a estimation types form.
 */
function ratingsystem_estimation_types_form($form, $form_state) {

  $result = db_select('ratingsystem_estimation_type', 'ret')
    ->fields('ret', array('id', 'name'))
    ->execute()
    ->fetchAll();

  $form = array();
  $rows = array();
  foreach ($result as $estimation_type) {
    $row = array();
    $row[] = $estimation_type->name;
    $row[] = l(t('Edit'), 'admin/config/ratingsystem/estimation/' . $estimation_type->id . '/edit')
    . '&emsp;' .
      l(t('Delete'), 'admin/config/ratingsystem/estimation/' . $estimation_type->id . '/delete');
    $rows[] = $row;
  }
  $output = theme('table', array(
    'rows' => $rows,
    'header' => array(t('Estimation type'), t('Actions')),
    'empty' => t('No estimation types in db now.'),
  ));

  $form['estimations'] = array(
    '#markup' => $output,
  );

  $form['new_estimation'] = array(
    '#type' => 'textfield',
    '#required' => true,
    '#title' => t('Enter name of new estimation to add it'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add estimation type')
  );

  return $form;
}

function ratingsystem_estimation_types_form_submit($form, &$form_state) {
  db_insert('ratingsystem_estimation_type')
    ->fields(array('name' => $form_state['values']['new_estimation']))
    ->execute();
  drupal_set_message(t('Estimation type "!type" added', array('!type' => $form_state['values']['new_estimation'])));
}

/**
 * Define a estimation types edit form.
 */
function ratingsystem_estimation_types_edit_form($form, $form_state, $type_id) {
  $result = db_select('ratingsystem_estimation_type', 'ret')
    ->fields('ret', array('id', 'name'))
    ->condition('id', $type_id)
    ->execute()
    ->fetchAll();

  $form = array();

  $form['estimation'] = array(
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => $result[0]->name,
    '#title' => t('Estimation type name'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  $form['type_id'] = array(
    '#type' => 'value',
    '#value' => $type_id
  );

  $form['cancel'] = array(
    '#markup' =>  l(t('Cancel'), 'admin/config/ratingsystem/estimation')
  );
  return $form;
}

/**
 * Define a estimation types edit form.
 */
function ratingsystem_estimation_types_edit_form_submit($form, &$form_state) {
  db_update('ratingsystem_estimation_type')
    ->fields(array('name' => $form_state['values']['estimation']))
    ->condition('id', $form_state['values']['type_id'])
    ->execute();
  drupal_set_message(t('Estimation type changed'));
  drupal_goto('admin/config/ratingsystem/estimation');
}


function ratingsystem_estimation_types_delete_form($form, &$form_state, $type_id) {
  return confirm_form(
    array(
      'type_id' => array(
        '#type' => 'value',
        '#value' => $type_id,
      ),
    ),
    t('Are you sure you want to remove this estimation type?'),
    'admin/config/ratingsystem/estimation',
    t('This action cannot be undone.'),
    t('Delete estimation type'),
    t('Cancel')
  );
}

function ratingsystem_estimation_types_delete_form_submit($form, &$form_state){
  db_delete('ratingsystem_estimation_type')
    ->condition('id', $form_state['values']['type_id'])
    ->execute();
  drupal_set_message(t('Estimation type deleted.'));
  drupal_goto('admin/config/ratingsystem/estimation');
}