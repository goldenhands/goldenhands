<?php
/**
 * @file
 * Administration page callbacks for the ratingsystem module.
 */

/**
 * Form builder. Configure rating system.
 *
 * @ingroup forms
 * @see system_settings_form().
 */

/**
 * Define a form.
 */
function ratingsystem_time_settings() {
  $value = variable_get('ratingsystem_days', 90);
  $form['ratingsystem_days'] = array(
    '#title' => t('Days'),
    '#type' => 'textfield',
    '#default_value' => $value,
    '#attributes' => array(
      'id' => 'ratingsystem_days'
    )
  );

  $form['ratingkoef'] = array(
    '#title' => t('Rating message reply koeficent'),
    '#type' => 'textfield',
    '#attributes' => array(
      'id' => 'ratingkoef'
    ),
    '#default_value' => variable_get('ratingkoef' , 20)
  );

  $form['month_count'] = array(
    '#title' => t('Month count'),
    '#type' => 'textfield',
    '#attributes' => array(
      'id' => 'month_count'
    ),
    '#default_value' => variable_get('month_count', 24)
  );

  $form['recom_koef'] = array(
    '#title' => t('Recommendation coefficient'),
    '#type' => 'textfield',
    '#default_value' => variable_get('recom_koef', 0.5)
  );

  return system_settings_form($form);
}

function ratingsystem_time_settings_validate($form, &$form_state) {
  if(!preg_match('/^[0-9]+$/', $form_state['values']['ratingsystem_days'])) {
    form_set_error('ratingsystem_days', t('You may input only numeric characters'));
  }
}