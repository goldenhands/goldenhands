<?php

function ratingsystem_deal_vote_form($form, $form_state, $deal_id, $type){
  $result = db_select('ratingsystem_estimation_type', 'ret')
    ->fields('ret', array('id', 'name'))
    ->execute();

  $form = array();

  $form['deal_id'] = array(
    '#type' => 'hidden',
    '#value' => $deal_id,
  );

  $form['vote_type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );

  if ($type == 'executor'){
    $form['description'] = array(
      '#markup'=>t('Set your estimate to deal executor. This estimates can\'t be changed.'),
    );
  }
  else if($type == 'customer'){
    $form['description'] = array(
      '#markup'=>t('Set your estimate to deal customer. This estimates can\'t be changed.'),
    );
  }
  foreach ($result as $estimation_type) {
    $form['type_' . $estimation_type->name] = array(
      '#type'=>'fivestar',
      '#title'=>t($estimation_type->name),
      '#required' => TRUE,
    );
  }

  if (module_exists('ec_advert')){
    $form['review_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Review text'),
      '#required' => TRUE,
    );
  }
  $form['submit'] = array(
    '#type'=>'submit',
    '#value'=>t('Save estimates'),
  );
  return $form;
}

function ratingsystem_deal_vote_form_submit($form, $form_state){
  $estimation_types = db_select('ratingsystem_estimation_type', 'ret')
    ->fields('ret', array('id', 'name'))
    ->execute();
  $deal = users_deal_get_deal($form_state['values']['deal_id']);
  if (!$deal) {
    drupal_set_message(t('Deal with number !deal_id not exists! Contact the site administrator.', array('!deal_id' => $form_state['values']['deal_id'])), 'error');
    return FALSE;
  }

  if ($form_state['values']['vote_type'] == 'executor'){
    $estimator_uid = $deal->executor_uid;
    $estimated_uid = $deal->customer_uid;
    $advert_pending_type = EC_ADVERT_EXECUTOR_REVIEW;
  }
  else{
    $estimator_uid = $deal->customer_uid;
    $estimated_uid = $deal->executor_uid;
    $advert_pending_type = EC_ADVERT_CUSTOMER_REVIEW;
  }

  foreach ($estimation_types as $estimation_type){
    db_insert('ratingsystem_estimation')
      ->fields(array(
      'estimator' => $estimator_uid,
      'estimated_uid' => $estimated_uid,
      'type' => $estimation_type->id,
      'deal_id' => $deal->deal_id,
      'nid' => $deal->advert_nid,
      'value' =>$form_state['values']['type_' . $estimation_type->name],
    ))
      ->execute();
  }

  if (isset($form_state['values']['review_text'])){
    db_insert('ec_advert_reviews')
      ->fields(array(
      'did' => $deal->deal_id,
      'nid' => $deal->advert_nid,
      'uid' => $estimator_uid,
      'text' =>$form_state['values']['review_text'],
      'changed' => REQUEST_TIME,
    ))
      ->execute();
  }

  module_invoke_all('advert_review_insert', $estimator_uid, $estimated_uid, $deal->advert_nid);

  db_delete('ec_advert_pending')
    ->condition('estimator', $estimator_uid)
    ->condition('type', $advert_pending_type)
    ->execute();

  drupal_set_message('Yor estimation has been saved. Thank you for your feedback.');

}