<?php
/**
 * @file
 * Administration page callbacks for the ratingsystem module.
 */

/**
 * Form builder. Configure rating system.
 *
 * @ingroup forms
 * @see system_settings_form().
 */

module_load_include('inc', 'ratingcalc', 'math');


/**
 * Define a settings form.
 */
function ratingsystem_settings_form($form, &$form_state) {
  $form = array();

  //drupal_add_css(drupal_get_path('module', 'ratingsystem') . '/ratingsystem.css');
  $form['form_description'] = array(
    '#markup' => t('Enter fraction for all user fields. Sum of the fields should not exceed 100%.')
  );

  $userObj = field_info_instances('user');
  foreach ($userObj as $elem) {
    foreach ($elem as $subElem) {
      $element_id = 'ratingsystem_field_' .$subElem['field_name'] . '_fraction';
      $form[$element_id] = array(
        '#type' => 'textfield',
        '#title' => $subElem['label'],
        '#default_value' => variable_get($element_id, 0),
        '#field_suffix' => t('%'),
        '#attributes' => array('style' => 'text-align:right'),
        '#size' => '5',
      );
    }
  }

  $form['ratingsystem_all_plenum_field_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value for 100% plenum fields'),
    '#default_value' => variable_get('ratingsystem_all_plenum_field_value', 0),
  );

  $form['ratingsystem_cron_count'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ratingsystem_cron_count', 30),
    '#title' => t('Number of adverts recalculated at cron.')
  );

  return system_settings_form($form);
}

/**
 * Validate a settings form.
 */
function ratingsystem_settings_form_validate($form, &$form_state) {
  $sum = 0;
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'ratingsystem_field_') === 0){
      $sum += (int)$value;
      $last_key = $key;
    }
  }

  if ($sum > 100){
    return form_set_error($last_key, 'Sum of the fields can not be more than 100%.');
  }

}