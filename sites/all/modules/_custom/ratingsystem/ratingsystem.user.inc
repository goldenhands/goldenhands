<?php

function _ratingsystem_user_view($uid) {
  $output = "";
  //Array of rating items
  $rating_arr = _ratingsystem_get_user_rating($uid);
  //User object
  $user = user_load($uid);
  $timestamp = $user->created;
  $date = date('d/m/Y' , $timestamp);
  //Recommendation coefficient
  $recom_koef = variable_get('recom_koef' , 0.5) ;
  $rate_phone = 10 ;
  $rate_network = 5 ;
  $acc_fill = 10;

  $phone_ind = $rating_arr['phone_confirmed'] ? t('Confirmed') : t('Not Confirmed');
  if ($rating_arr['network'] == 0) {
    $network_count = 0;
  }
  elseif (!empty($user->field_phone['und'][0]['value'])) {
    $net = $rating_arr['network'] - $rate_phone ;
    $network_count = $net / $rate_network ;
  }
  else {
    $rating_arr['network'];
    $network_count = $rating_arr['network'] / $rate_network;
  }

  $output .= '<dl>';
  $output .= '<dt>1.'.t('Table filling').'</dt>';
  $output .= '<dd>' . t('Account filling procent') . " " . $rating_arr['table_filling'] . '</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['table_filling'] . '/' . $acc_fill .  '</dd>';
  $output .= '<dt>2.' . t('Deals') . '</dt>';
  $output .= '<dd>' . t('Deals count') . " " . $rating_arr['number_of_deals'] . '</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['number_of_deals'] . '</dd>';
  $output .= '<dt>3.' . t('Messages reply') . '</dt>';
  $output .= '<dd>' . t('Message count') . " " . $rating_arr['messData']['all_mess'] . '</dd>';
  $output .= '<dd>' . t('Reply procent') . " " . round($rating_arr['messData']['prc'] , 2) . '%'.'</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . round($rating_arr['messData']['messData'], 0) . '</dd>';
  $output .= '<dt>4.' . t('Recommendations') . '</dt>';
  $output .= '<dd>' . t('Recommendations count') . " " . $rating_arr['recomm']*2 . '</dd>';
  $output .= '<dd>' . t('Recommendations coefficient') . " " . $recom_koef . '</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['recomm'] . '</dd>';
  $output .= '<dt>5.' . t('Registration time') . '</dt>';
  $output .= '<dd>' . t('Registration date') . " " . $date . '</dd>';
  $output .= '<dd>' . t('Full months') . " " .  $rating_arr['monthCounter'] . '</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['monthCounter'] . '</dd>';
  $output .= '<dt>6.' . t('Contacts') . '</dt>';
  $output .= '<dd>' . t('Phone status') . " <strong>" . $phone_ind . '</strong></dd>';
  $output .= '<dd>' . t('Rate for phone') . " <strong>" . $rate_phone . '</strong></dd>';
  $output .= '<dd>' . t('Networks') . " <strong>" . $network_count . '</strong></dd>';
  $output .= '<dd>' . t('Rate for each network') . " <strong>" . $rate_network . '</strong></dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['network'] . '</dd>';
  $output .= '<dt>7.' . t('Rating from admin') . '</dt>';
  $output .= '<dd>' . t('Rating from admin') . " " . $rating_arr['attent'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . l(t('Edit'), 'user/' . $uid . '/rating/edit') . '</dd>';
  $output .= '<dd class="bold">' . t('Total') . " " . $rating_arr['attent'] . '</dd>';
  $output .= '<dt>8.' . t('Total User Rating') . '</dt>';
  $output .= '<dd class="bold">' . t('Total User Rating') . " " . round($rating_arr['all'], 0) . '</dd>';
  $output .= '</dl>';

return $output;
}

/**
 * Считаем рейтинг ползователя и возврашаем
 *
 * @param $userId : ID данного пользователя
 * @return array : массив с данными о рейтинге и общий рейтинг
 */
function _ratingsystem_get_user_rating($userId) {
  //баллы за регистрацию
  /* todo move to advert rating
  $currentDate = time();
  $ored = db_or();
  $ored->condition('customer_uid', $userId)->condition('executor_uid', $userId);
  $existingUser = user_load($userId);
  $db = db_select('users_deal', 'd')
    ->fields('d', array('date_created'))
    ->condition($ored)
    ->condition('date_created', ($currentDate - ((variable_get('ratingsystem_days', 90)) * 86400)), '>');
  $second_result = $db->countQuery()
    ->execute()
    ->fetchCol();
  //Считаем количество сделок
  $number_of_deals = (empty($second_result[0]) ? 0 : $second_result[0]);*/

  $user_ent = entity_load('user', array($userId));
  $userObj = field_info_instances('user');
  //Количество подключенных соцсетей
  $networks = 0;

  $all_plenum_fields = variable_get('ratingsystem_all_plenum_field_value', 0);
  foreach ($user_ent as $user_e) {
    $phone_confirmed = false;
    //Считаем на сколько пользователь заполнил свои данные
    $table_filling = 0;
    if (($user_e->picture) != NULL)
      //добавляем баллы за аватар
      $table_filling += variable_get('picture_slider_store', 0);
      foreach ($userObj as $elem) {
        foreach ($elem as $subElem) {
          //$und_array = $user_e->$subElem['field_name'];
          $elem_id = 'ratingsystem_field_' . $subElem['field_name'] . '_fraction';
          //if (!empty($und_array))
            //watchdog('d','<pre>' . var_export($und_array,true) .  '</pre>');
            //$elem_id = 'ratingsystem_field_' . $user_e->$subElem['field_name'] . '_fraction';
            $table_filling += variable_get($elem_id, 0) * $all_plenum_fields / 100;
        }
      }
    //end foreach

    //test for phone number
    /* todo change to db_select */
    if (!empty($user_e->field_phone) && !empty($user_e->sms_custom['number']) && $user_e->sms_custom['number'] == $user_e->field_phone[LANGUAGE_NONE][0]['value'] && $user_e->sms_custom['status'] == SMS_CUSTOM_PHONE_CONFIRMED){
      $networks += 10;
      $phone_confirmed = true;
    }

    $attent = db_select('ratingsystem_user_rating', 'ur')
      ->fields('ur', array('additional'))
      ->condition('uid', $userId)
      ->execute()->fetchField();
    if (is_null($attent) || $attent === FALSE) $attent = 0;

  }
  //end first foreach

  //$table_filling /= 10;

  //todo change code. leave only facebook
  $people_networks_types = array('vkontakte', 'odnoklassniki', 'facebook', 'mailru', 'yandex', 'google', 'livejournal', 'twitter', 'openid');
  foreach ($people_networks_types as $social_net_one) {
    $db = db_select('authmap', 'a')->
      fields('a', array('module'))
      ->condition('authname', '%' . db_like($social_net_one) . '%', 'LIKE')
      ->condition('uid', $userId);
    $tmp_networks = $db->countQuery()
      ->execute()
      ->fetchCol();
    if ($tmp_networks[0] > 0)
      $networks += 5;
  }

  //Считаем как долго пользователь находитяс на сайте
  /* todo move to advert rating
  $startDate = db_select('users', 'us')
    ->fields('us', array('created'))
    ->condition('uid', $userId)
    ->execute()
    ->fetchField();
  $currentDateTime = new DateTime("now");
  $startDateTime = date_create('@' . $startDate);
  $monthCounter = date_diff($currentDateTime, $startDateTime);
  $monthCounter = $monthCounter->m;

  $def_month = variable_get('month_count', 24);
  $monthCounter = (($monthCounter > $def_month) ? $def_month : $monthCounter);*/

  $db = db_select('user_recommendation', 'ur')
    ->fields('ur', array('rid'))
    ->condition('about_uid', $userId);
  $second_result = $db->countQuery()->execute()->fetchCol();

  //Считаем рекомендации ползьователя
  $recomm = $second_result[0] / 2;


  //сраднее значение оценок для данного пользователя
  /* todo move to advert rating
  $estimations_values = 0;
  $db = db_select('user_estimations', 'est');
  $db->addExpression('AVG(est.value)');
  $db->condition('author_of_estimation', $userId);
  $db->groupBy('est.estim_id');
  $result = $db->execute()->fetchCol();
  foreach ($result as $row)
    $estimations_values += round($row);
*/
  //Получаем рейтинг пользователя за ответы на письма запысиваем в спецальное поле
  $messData = _ratingsystem_get_user_answer_percent($userId, REQUEST_TIME);

  //$all = $number_of_deals + $attent + $table_filling + $networks + $monthCounter + $recomm + $messData['messData'] + $estimations_values;
  $all = $attent + $table_filling + $networks +  $recomm + $messData['messData'];

  $ratingArr = array(
    'attent' => $attent,
    'table_filling' => $table_filling,
    'network' => $networks,
    'phone_confirmed' => $phone_confirmed,
    'recomm' => $recomm,
    'messData' => $messData,
    'all' => $all
  );

  return $ratingArr;
}

/**
 * Функция считающая количество ответов на письма
 *
 * @param $userId
 * @param $currentDate
 * @return int
 */
function _ratingsystem_get_user_answer_percent($userId, $currentDate){
  $resultMessages = 0;
  $arrivedMessages = 0;

  $db = db_select('user_dialogs_threads', 't');
  $db->innerJoin('user_dialogs_messages', 'm', 't.thread_id = m.thread_id AND '.$userId.' <> m.author AND t.user2 = '.$userId);
  $db->fields('t', array('thread_id'));
  $db->fields('m', array('author'));
  $db->condition('timestamp', ($currentDate - ((variable_get('ratingsystem_days', 90)) * 86400)), '>');
  $db->addExpression('MIN(m.timestamp)', 'minn');
  $db->groupBy('t.thread_id');
  $ress = $db->execute();
  while($roww = $ress->fetchAssoc()) {
    $query = db_select('user_dialogs_messages', 'ms');
    $query->addExpression('MIN(ms.timestamp)', 'minimum');
    $query->condition('thread_id', $roww['thread_id']);
    $query->condition('author', $userId);
    $query->condition('type', 1); //only real messages, expect system
    $query->condition('timestamp', $roww['minn'], '>');
    $secRes = $query->execute()->fetchCol();
    $secRes = $secRes[0];
    $noAnswerFlag = FALSE;
    if(!empty($secRes)){
      // Если ответил позже чем на 3 дня , ответ не считеам в рейтинге
      if(($secRes - $roww['minn']) > (3 * 86400))
        $noAnswerFlag = TRUE;
      $db2 = db_select('user_dialogs_messages', 'msg');
      $db2->fields('msg', array('thread_id'));
      $db2->fields('msg', array('author'));
      $db2->fields('msg', array('timestamp'));
      $db2->condition('thread_id', $roww['thread_id']);
      $db2->condition('type', 1); //only real messages, expect system
      $db2->orderBy('timestamp', 'ASC');
      $finalRes = $db2->execute();
      $prevUser = -1;
      $startlag = FALSE;
      while ($rdrw = $finalRes->fetchAssoc()) {
        if ($startlag) {
          if($rdrw['author'] <> $userId) {
            $arrivedMessages++;
          }
          if(($rdrw['author'] == $userId) && ($prevUser <> $userId))
            $resultMessages++;
        }
        if(($rdrw['thread_id'] == $roww['thread_id']) && ($rdrw['author'] == $roww['author']) && ($rdrw['timestamp'] == $roww['minn'])) {
          $startlag = TRUE;
          $arrivedMessages++;
        }
        $prevUser = $rdrw['author'];
      }
    }
    if($noAnswerFlag)
      $resultMessages--;
  }
  //Коэфицент максимум 20
  $koef = variable_get('ratingkoef', 20);
  $messkoeff = (($arrivedMessages > $koef) ? $koef: $arrivedMessages);
  //Считаем процент ответов во время
  $prc = (( (($resultMessages == 0) && ($arrivedMessages == 0)) ? 0 : ((100 * $resultMessages) / $arrivedMessages)));
  //Умножаем на коэфицент ответов
  $messData = ($messkoeff * $prc) / 100;

  $mess_arr =array(
    'messkoef' => $messkoeff,
    'prc' => $prc,
    'messData' => $messData,
    'all_mess' => $arrivedMessages,
  );

  return $mess_arr;
}


function _ratingsystem_get_prev_month(&$currMonth, &$currYear) {
  $res = 0;
  if (--$currMonth == 0) {
    $currYear--;
    $currMonth = 12;
  }
  if (($currMonth == 1) || ($currMonth == 3) || ($currMonth == 5) || ($currMonth == 7) || ($currMonth == 8) || ($currMonth == 10) || ($currMonth == 12))
    $res = 31;
  else
    if (($currMonth == 4) || ($currMonth == 6) || ($currMonth == 9) || ($currMonth == 11))
      $res = 30;
    else
      if ($currMonth == 2)
        $res = ((($currYear % 4) == 0) ? 29 : 28);
  return $res;
}

function _ratingsystem_user_edit_rating_form($form, $form_state, $uid){
  $form = array();
  $res = db_select('ratingsystem_user_rating', 'ur')
    ->fields('ur', array())
    ->condition('uid', $uid)
    ->execute()->fetchAssoc();
  $form['user_rating']['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );
  $cur_user = user_load($uid);
  $form['user_rating']['name']['#markup'] = t('User') . ': ' . format_username($cur_user) . '<br>';
  $form['user_rating']['rating']['#markup'] = t('Calculated Rating') . ': ' . $res['value'] . '<br>';
  $form['user_rating']['additional'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional Rating'),
    '#default_value' => $res['additional']
    );
  $form['user_rating']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function _ratingsystem_user_edit_rating_form_submit($form, $form_state){
  $additional = ($form_state['values']['additional'] == '') ? null : $form_state['values']['additional'];
  db_update('ratingsystem_user_rating')
    ->fields(array('additional' =>  $additional))
    ->condition('uid', $form_state['values']['uid'])
    ->execute();

  drupal_set_message(t('Additioanl user rating has been updated'));
  drupal_goto('user/' . $form_state['values']['uid'] . '/rating');
}

/**
 * Implementation of hook_calc(). ???
 */
function ratingsystem_calc_user($userId) {

  if($userId == 'all') {
    //calc for all users
    $res = db_select('users', 'u')->fields('u', array('uid'))->condition('status', 1)->execute();
    foreach ($res as $row) {
      _ratingsystem_set_rating_for_user($row->uid);
    }
    drupal_set_message(t('All user rating recalculated.'));
    return '';
  }
  else {
    _ratingsystem_set_rating_for_user($userId);
  }
}

/**
 * Сохраняем рейтинг пользователя
 *
 * @param $userId
 * @return void
 */
function _ratingsystem_set_rating_for_user($userId) {

  //Получаем массив с данными рейтинга
  $rating_arr = _ratingsystem_get_user_rating($userId);

  //insert or update records
  db_merge('ratingsystem_user_rating')
    ->key(array('uid' =>  $userId))
    ->fields(array(
      'value' => $rating_arr['all'],
      'last_calc' => time(),
    ))
    ->execute();
  //Загружаем пользователя
  $existingUser = user_load($userId);
  watchdog('RatingCalc', 'Rating calculated for user: ' . $existingUser->name . " (Id = " . $existingUser->uid . ")" );
}