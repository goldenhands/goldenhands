<?php
/**
 * @file
 * Administration page callbacks for the ratingsystem module.
 */

/**
 * Form builder. Configure rating system.
 *
 * @ingroup forms
 * @see system_settings_form().
 */




/**
 * Define a form.
 */
function ratingsystem_user_estimations() {
  global $user;
  $template_for_estimation_control = array(0, 1, 2, 3, 4, 5);
  $estimation_types_list = $results = array();
  $generate_output_flag = FALSE;
  $prev_id = -1;
  $prev_user = -1;
  module_load_include('inc', 'ratingcalc', 'math');

  $db = db_select('user_estimation_types', 'uu')->fields('uu', array('id', 'name'));
  $result = $db->execute()->fetchAll();
  foreach ($result as $row)
    $estimation_types_list[] = array($row->id, $row->name);

  $customer_uids = get_deals_by_uid($user->uid, 'customer_uid');
  $executor_uids = get_deals_by_uid($user->uid, 'executor_uid');

  if(!empty($customer_uids) && !empty($executor_uids)){
    foreach($customer_uids as $customer_uid){
        $results[] = array('deal_id' => $customer_uid->deal_id, 'obtained_the_estimate' => $customer_uid->executor_uid, 'executor_uid' => $customer_uid->executor_uid);
      foreach($executor_uids as $executor_uid){
        $results[] = array('deal_id' => $executor_uid->deal_id, 'obtained_the_estimate' => $executor_uid->customer_uid, 'customer_uid' => $executor_uid->customer_uid);
      }
    }
  }
  elseif(!empty($customer_uids) && empty($executor_uids)){
    foreach($customer_uids as $customer_uid){
      $results[] = array('deal_id' => $customer_uid->deal_id, 'obtained_the_estimate' => $customer_uid->executor_uid, 'executor_uid' => $customer_uid->executor_uid);
    }
  }
  elseif(empty($customer_uids) && !empty($executor_uids)){
    foreach($executor_uids as $executor_uid){
      $results[] = array('deal_id' => $executor_uid->deal_id, 'obtained_the_estimate' => $executor_uid->customer_uid, 'customer_uid' => $executor_uid->customer_uid);
    }
  }

  foreach ($results as $row) {
    foreach($estimation_types_list as $atom_estimation) {
      //$row->deal_id
      $db2 = db_select('user_estimations', 's')->fields('s', array('value'))->condition('deal_id', $row['deal_id'])->condition('estim_id', $atom_estimation[0])->condition('author_of_estimation', $user->uid);
      $second_result = $db2->countQuery()->execute()->fetchCol();
      if($second_result[0] == 0)

        db_insert('user_estimations')->fields(array('deal_id' => $row['deal_id'], 'estim_id' => $atom_estimation[0], 'author_of_estimation' => $user->uid, 'obtained_the_estimate' => $row['obtained_the_estimate']))->execute();
      ///////////////////////////////////////////
      $db2 = db_select('user_estimations', 's')->fields('s', array('value'))->condition('deal_id', $row['deal_id'])->condition('estim_id', $atom_estimation[0])->condition('author_of_estimation', $user->uid);
      $second_result = $db2->execute()->fetchCol();
      $visavi_id = -1;
      $generate_output_flag = TRUE;
      $customer_uid = isset($row['customer_uid']) ? $row['customer_uid'] : NULL;
      if($customer_uid <> $user->uid) {
        $visavi_id = $customer_uid;
      } else {
        $visavi_id = $row['executor_uid'];
      }

      $userObj = field_info_instances('user');

      $output = '';
      if ($prev_id <> $row['deal_id']){
        $output .= '<div style="margin-top: 20px; margin-bottom: 15px; font-weight: bold;">' . t('Identifier of deal: ') . ' ' . $row['deal_id'] . '</div>';
        $prev_user = -1;
      }

      if ($prev_user <> $visavi_id) {

      $output .= '<div>' . t('The user with which the transaction is concluded') . '</div>';
      $user_ent = entity_load('user', array($visavi_id));
      foreach ($userObj as $elem) {
        foreach ($elem as $subElem) {
          foreach ($user_ent as $user_c_fields) {
            if (!empty($user_c_fields->$subElem['field_name'])) {
              if(permissions_for_this_field_are_changed($subElem['field_name']) == FALSE) {
                $field_val = $user_c_fields->$subElem['field_name'];
                foreach($field_val['und'][0] as $key => $val) {
                  if($key == 'value') {
                    $output .= '<div>'. $subElem['label'].': '. $val.'</div>';
                  }
                }
              }
            }
          }
        }
      }
      //end foreach


      }
      $prev_user = $visavi_id;

      $form['user_display_'. $row['deal_id'].'_'. $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      $output = '<div class="user_estim_form_wrap">';
      $form['user_before_control_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      $output = '<div class="user_estim_form1">';
      $form['user_before_control_2_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      $form['estimation_control_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'select',
        '#options' => $template_for_estimation_control,
        '#value' => $second_result[0],
        '#attributes' => array(
          'id' => 'estimation_control_' . $row['deal_id'] . '_' . $atom_estimation[0]
        )
      );

      $output = '</div>';
      $form['user_after_control_2_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      $output = '<div class="user_estim_form2">' . $atom_estimation[1] . '</div>';
      $form['user_before_control_3_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      $output = '</div>';
      $form['user_after_control_' . $row['deal_id'] . '_' . $atom_estimation[0]] = array(
        '#type' => 'markup',
        '#markup' => $output
      );

      ///////////////////////////////////////////

      $prev_id = $row['deal_id'];
    }


  }

  $db = db_select('user_estimations', 'et')->fields('et', array('deal_id', 'estim_id'));
  $result = $db->execute()->fetchAll();
  foreach ($result as $row) {
    if(isset($_POST['estimation_control_'. $row->deal_id.'_'. $row->estim_id])) {
      db_update('user_estimations')->fields(array('value' => $_POST['estimation_control_' . $row->deal_id . '_' . $row->estim_id]))->condition('estim_id', $row->estim_id)->condition('deal_id', $row->deal_id)->condition('author_of_estimation', $user->uid)->execute();
    }
  }

  $db = db_select('user_estimations', 't1');
  $db->distinct();
  $db->leftJoin('user_estimation_types', 't2', 't1.estim_id = t2.id');
  $db->isNull('t2.id');
  $db->fields('t1', array('estim_id'));
  $result = $db->execute()->fetchAll();
  foreach ($result as $row)
    db_delete('user_estimations')->condition('estim_id', $row->estim_id)->execute();

  if($generate_output_flag) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
    );
    return $form;
  }
}