(function ($) {
    Drupal.behaviors.userDialogTable = {
        attach:function ($context) {
            $('.user-dialog-clicked').click(function () {
                location.href = 'dialogs/threads/' + this.id;
            });
        }
    };

    Drupal.behaviors.userDialogReplay = {
        attach:function ($context) {
            $('.form-item-body #edit-body').keyup(function () {
                checkText();
            });

        }
    };

    function checkText() {
        var description_text = '';
        if ($('.form-item-body #edit-body').first().val().length < window.Drupal.settings.dialog_reply['max_length_sms']) {
            description_text = window.Drupal.settings.dialog_reply['text_less'];
        }
        else {
            description_text = window.Drupal.settings.dialog_reply['text_more'];
        }
        $('.form-item-body .description').first().html(
            description_text
        );
    }

}(jQuery));