/*
 file not used now
 remove before deploy if ajax not needed for this module

 */
(function ($) {
    Drupal.behaviors.user_recommenation = {
        attach:function (context, settings) {
            $(document).delegate('a.ajax-publish-recommendation', 'click', null, function () {
                $.post('/user/vouch/publish/' + ($(this).attr('id')), null, publishVouch);
                return false;
            });


            $(document).delegate('a.ajax-delete-recommendation', 'click', null, function () {
                $.post('/user/vouch/delete/' + ($(this).attr('id')), null, deleteVouch);
                return false;
            });


            //todo refactor code
            var tab = $('#reviews-tabs-div');
            if (tab.length == 0){
                $('#block-ec-advert-advert-reviews .content').addClass('tab-content-active');
                $('a#block-ec-advert-advert-reviews').addClass('active');
                //move block headers to tabs
                $('#block-ec-advert-advert-reviews h2 a').attr('id', 'block-ec-advert-advert-reviews');
                $('#block-ec-advert-advert-reviews').before('<div class="tabs-div" id="reviews-tabs-div">' + $('#block-ec-advert-advert-reviews h2').html() + $('#block-user-recommendation-user-recommendation-info h2').html() + '</div>');
                $('#block-ec-advert-advert-reviews h2 a').remove();
                $('#block-user-recommendation-user-recommendation-info h2 a').remove();

                $('.recommendations .hidden-recommendations').hide();
                $('.recommendations .toggler.expanded').hide();
                $('.recommendations .toggler.collapsed').append(' (' + $('.recommendations #recommendations-count').html() + ')');

                $('.recommendations .toggler').click(function (e) {
                    //var hiddens = $('.recommendations .hidden-recommendations');
                    if ($('.recommendations .hidden-recommendations').is(":visible")) {
                        $('.recommendations .toggler.collapsed').show();
                        $('.recommendations .toggler.expanded').hide();
                        $('.recommendations .hidden-recommendations').hide();
                    }
                    else {
                        $('.recommendations .toggler.collapsed').hide();
                        $('.recommendations .toggler.expanded').show();
                        $('.recommendations .hidden-recommendations').show();
                    }
                });
            }
            $('a.tab-header').click(function (e) {
                ie8SafePreventEvent(e);
                $('.tabs-div').find('.active').removeClass('active');
                $('.tab-content-active').removeClass('tab-content-active');
                var id_tab = $(this).attr('id');
                $(this).addClass('active');
                $("div#" + id_tab + ' .content').addClass('tab-content-active');
            });
            function ie8SafePreventEvent(e) {
                if (e.preventDefault) {
                    e.preventDefault()
                }
                else {
                    e.stop()
                }
                ;
                e.returnValue = false;
                e.stopPropagation();
            }


        }
    };

})(jQuery);