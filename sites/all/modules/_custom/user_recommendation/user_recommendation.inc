<?php

/*
 * Callback for publish recommendation
 */
function user_recommendation_vouch_publish($rid) {
  db_update('user_recommendation')
    ->fields(array('published' => 1))
    ->condition('rid', $rid)
    ->execute();
  global $user;
  drupal_set_message(t('Recommendation is published.'));
  drupal_goto('user/' . $user->uid . '/opinions/recommendations/vouch');
}

/*
 * Callback for delete recommendation
 */
function user_recommendation_vouch_delete($rid) {
  db_delete('user_recommendation')
    ->condition('rid', $rid)
    ->execute();
  global $user;
  //drupal_set_message('Рекомендация удалена.');
  drupal_set_message(t('Recommendation deleted.'));
  drupal_goto('user/' . $user->uid . '/opinions/recommendations/vouch');

  /*$output =  'Не опубликовано (' . l('Опубликовать', 'user/vouch/publish/'.$rid, array('attributes' => array('class' => 'ajax-publish-recommendation', 'id'=>$rid ))) . ')';
  drupal_json_output(array('status' => 0, 'id'=> $rid));*/
}

function user_recommendation_request_vouch($form, $form_state, $account) {

  $sid = 'recommendation_token_' . $account->uid;
  $cache = cache_get($sid);
  if ($cache !== FALSE) {
    $recommendation_token = $cache->data;
  }
  else {
    $recommendation_token = db_select('user_recommendation_tokens', 'urt')
      ->condition('uid', $account->uid)
      ->fields('urt', array('token'))
      ->execute()
      ->fetchField();
    cache_set($sid, $recommendation_token);
  }

  if ($recommendation_token == FALSE) {
    //need generate token
    $recommendation_token = user_password();
    db_insert('user_recommendation_tokens')
      ->fields(array(
      'uid' => $account->uid,
      'token' => $recommendation_token,
    ))
      ->execute();
    cache_set($sid, $recommendation_token);
  }
  ;

  global $base_url;
  $form['recommenation_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Share this link'),
    '#description' => t('Send this link to your friends, that they can add recommendation for you'),
    '#value' => $base_url . '/user/add/vouch/' . $account->uid . '?r=' . $recommendation_token,
    '#size' => 104,
  );

  $form['mail_to'] = array(
    '#type' => 'textarea',
    '#title' => t('Send to friend'),
    '#description' => t('Enter 10  e-mail addresses separated by comma. We send email every people with your link.'),
    '#default_value' => ''
  );

  $form['send'] = array(
    '#type' => 'submit',
    //'#value' => 'Отправить',
    '#value' => t('Send'),
  );

  return $form;
}

function user_recommendation_request_vouch_validate($form, $form_state) {
  if (trim($form_state['values']['mail_to']) == '') {
    form_set_error('mail_to', 'Необходимо ввести хотя бы один адрес.');
    form_set_error('mail_to', 'You must enter at least one email.');
  }
}

function user_recommendation_request_vouch_submit($form, $form_state) {
  global $user;
  $recipients = explode(',', $form_state['values']['mail_to']);
  //$params['subject'] = t('Запрос рекомендации от пользователя ' . $user->name);
  $params['subject'] = t('Request recommendations from the user ' . $user->name);
  //replace token and add recommendation link to mail body
  $params['body'] = token_replace(variable_get('user_recommendation_mail_template', ''), array('user' => $user));
  $params['body'] = t($params['body'], array('!recommendation_url' => $form['recommenation_link']['#value']));
  $language = $user ? user_preferred_language($user) : language_default();
  $success = array();
  $failure = array();
  foreach ($recipients as $recipient) {
    $mail = drupal_mail('user_recommendation', 'user_recommendation', $recipient, $language, $params);
    if ($mail['result']) {
      watchdog('user_recommendation', t('Mail !subject sent to !to ', array(
        '!subject' => $params['subject'],
        '!to' => $recipient
      )));
      $success[] = $recipient;
    }
    else {
      $failure[] = $recipient;
    }
  }
  if (count($success) > 0) {
    drupal_set_message(t('Request sent successfully !to ', array('!to' => implode(',', $success))));
  }
  if (count($failure) > 0) {
    drupal_set_message(t('Error at sent to !to ', array('!to' => implode(',', $failure))), 'error');
  }
}


function user_recommendation_manage_vouch_list($account) {
  $header = array(t('Author'), t('Text'), t('Actions'));
  $recommendations = db_select('user_recommendation', 'ur')
    ->fields('ur', array('rid', 'from_uid', 'about_uid', 'text', 'published'))
    ->condition('about_uid', $account->uid)
    ->condition('published', 0)
    ->execute();
  $rows = array();
  foreach ($recommendations as $recommendation) {
    $cells = array();
    $author = user_load($recommendation->from_uid);
    $cells[] = l($author->name, 'user/' . $author->uid);
    $cells[] = $recommendation->text;
    $cells[] = l(t('Publish'), 'user/vouch/publish/' . $recommendation->rid, array(
      'attributes' => array(
        'class' => 'ajax-publish-recommendation',
        'id' => $recommendation->rid
      )
    )) .
      ' ' . l(t('Delete'), 'user/vouch/delete/' . $recommendation->rid, array(
      'attributes' => array(
        'class' => 'ajax-delete-recommendation',
        'id' => $recommendation->rid
      )
    ));
    $rows[] = $cells;
  }
  //drupal_add_js(drupal_get_path('module', 'user_recommendation') . '/user_recommendation.js');
  $pending = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'caption' => 'New recommendations',
    'empty' => 'You have no recommendations yet.'
  ));

  $recommendations = db_select('user_recommendation', 'ur')
    ->fields('ur', array('rid', 'from_uid', 'about_uid', 'text', 'published'))
    ->condition('about_uid', $account->uid)
    ->condition('published', 1)
    ->execute();
 // $rows = array();
  $output='<h3>'.t('All recommendations:').'<h3>';
  if (count($recommendations)==0){
    $output .= 'You have no recommendations.';
  }
  foreach ($recommendations as $recommendation) {
   // $cells = array();
    $author = user_load($recommendation->from_uid);
    if ($author) {
      /*
    $cells[] = l($author->name, 'user/' . $author->uid);
    $cells[] = $recommendation->text;
    $cells[] = l(t('Delete'), 'user/vouch/delete/' . $recommendation->rid, array(
      'attributes' => array(
        'class' => 'ajax-delete-recommendation',
        'id' => $recommendation->rid
      )
    ));
    $rows[] = $cells;
  }*/
      $filepath = ec_user_get_avatar_url($author->uid);
      $output .= '<div class="review-block">';
      $output .= '<div class="review-div-avatar-with-arrow">';
      $output .= '<div class = "review-author-avatar">' . l(theme('image_style', array('style_name' => 'search_result_author_avatar', 'path' => $filepath, 'alt' => $author->name, 'title' => $author->name)), 'user/' . $author->uid, array('html' => TRUE)).'</div>' ;
      $output .='<span class="review-text-left"></span>';
      $output .= '</div>';//end of review-div-avatar-with-arrow
      $output .= '<div class="review-text-zone">';
      $output .= '<div class="review-text">';
      $output .= '<div class="review-author-name">';
      $output .= l(theme('username', array('account'=>$author)),'user/' . $author->uid, array('html' => TRUE));
      $output .= '</div>';//end of author name
      $output .= $recommendation->text;
      $output .= '<div class="review-date">' .  format_date((int) $recommendation->published, 'custom', 'd M Y') . '</div></div>';
      $output .= '</div>';//end of text zone
      $output .= '</div>';//end of block
  }
  //drupal_add_js(drupal_get_path('module', 'user_recommendation') . '/user_recommendation.js');
 /* $pudlished = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'caption' => t('All recommendations'),
    'empty' => t('You have no recommendations yet.')
  ));*/
  }
  return $pending . $output;
}
function user_recommendation_my_vouch($account) {
  $header = array(t('User'), t('Text'), t('Actions'));
  $recommendations = db_select('user_recommendation', 'ur')
    ->fields('ur', array('rid', 'from_uid', 'about_uid', 'text', 'published'))
    ->condition('from_uid', $account->uid)
    ->execute();


  $output='<h3>'.t('Your recommendations:').'<h3>';
  if (count($recommendations)==0){
    $output .= 'You have not written recommendations yet.';
  }
  foreach ($recommendations as $recommendation) {
    $author = user_load($recommendation->from_uid);
    if ($author) {
      $filepath = ec_user_get_avatar_url($author->uid);
      $output .= '<div class="review-block">';
      $output .= '<div class="review-div-avatar-with-arrow">';
      $output .= '<div class = "review-author-avatar">' . l(theme('image_style', array('style_name' => 'search_result_author_avatar', 'path' => $filepath, 'alt' => $author->name, 'title' => $author->name)), 'user/' . $author->uid, array('html' => TRUE)).'</div>' ;
      $output .='<span class="review-text-left"></span>';
      $output .= '</div>';//end of review-div-avatar-with-arrow
      $output .= '<div class="review-text-zone">';
      $output .= '<div class="review-text">';
      $output .= '<div class="review-author-name">';
      $output .= l(theme('username', array('account'=>$author)),'user/' . $author->uid, array('html' => TRUE));
      $output .= '</div>';//end of author name
      $output .= $recommendation->text;
      $output .= '<div class="review-date">' .  format_date((int) $recommendation->published, 'custom', 'd M Y') . '</div></div>';
      $output .= '</div>';//end of text zone
      $output .= '</div>';//end of block
    }
  }
 // $rows = array();
 /* foreach ($recommendations as $recommendation) {
    $cells = array();
    $about_user = user_load($recommendation->about_uid);
    $cells[] = l($about_user->name, 'user/' . $about_user->uid);
    $cells[] = $recommendation->text;
    $cells[] = l(t('Delete'), 'user/vouch/delete/' . $recommendation->rid, array(
      'attributes' => array(
        'class' => 'ajax-delete-recommendation',
        'id' => $recommendation->rid
      )
    ));
    $rows[] = $cells;
  }
  $pudlished = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'caption' => t('My recommendations'),
    'empty' => t('You have not written recommendations yet.')
  ));*/
  return $output;
}

function user_recommendation_add_vouch($form, &$form_state, $uid) {
  $to_user = user_load($uid);
  if ($to_user === FALSE) {
    $form['error'] = array('#markup' => t('User, which you are want to write a recommendation does not exist.'));
    return $form;
  }

  global $user;
  if ($user->uid == $uid) {
    $form['error'] = array('#markup' => t('You can not add recommendation to yourself.'));
    return $form;
  }
  //check user recommendation token
  //retrieve from cache or db
  $sid = 'recommendation_token_' . $uid;
  $cache = cache_get($sid);
  if ($cache !== FALSE) {
    $recommendation_token = $cache->data;
  }
  else {
    $recommendation_token = db_select('user_recommendation_tokens', 'urt')
      ->condition('uid', $uid)
      ->fields('urt', array('token'))
      ->execute()
      ->fetchField();
    cache_set($sid, $recommendation_token);
  }
  //check token and url param
  if (!isset($_GET['r']) || $_GET['r'] != $recommendation_token) {
    //$form['error'] = array('#markup' => t('Неверный код доступа для написания рекомендации.'));
    $form['error'] = array('#markup' => t('Invalid access code for adding recommendation.'));
    return $form;
  }

  //is recommendation about this user exists and authot is current user
  $recommendations = db_select('user_recommendation', 'ur')
    ->condition('from_uid', $user->uid)
    ->condition('about_uid', $uid)
    ->fields('ur', array('rid'))
    ->execute()
    ->rowCount();
  if ($recommendations > 0) {
    //drupal_set_message('Вы уже оставили рекомендацию этому пользователю.');
    drupal_set_message(t('You have already added a recommendation to this user.'));
    drupal_goto('user/' . $user->uid . '/opinions/recommendations/vouch/my');
    return $form;
  }

  $form['about_uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );

  $form['recommendation_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Your recommendation for ') . $to_user->name,
    '#default_value' => ''
  );

  $form['add_vouch'] = array(
    '#type' => 'submit',
    '#value' => t('Add recommendation'),
  );

  return $form;
}

function user_recommendation_add_vouch_validate($form, &$form_state) {
  if (trim($form_state['values']['recommendation_text']) == '') {
    form_set_error('recommendation_text', t('Recommendation can not be empty'));
  }
}

function user_recommendation_add_vouch_submit($form, &$form_state) {
  global $user;
  db_insert('user_recommendation')
    ->fields(array(
    'from_uid' => $user->uid,
    'about_uid' => $form_state['values']['about_uid'],
    'text' => $form_state['values']['recommendation_text'],
    'published' => 0,
  ))
    ->execute();

  drupal_set_message(t('Your recommendation has been added.'));
  $form_state['redirect'] = 'user';
}