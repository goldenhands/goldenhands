(function ($) {
    Drupal.behaviors.smsUser = {
        attach: function($context) {
            $('#city_list').bind('change', function(e) {
                $.ajax({
                    url: '/ip_auto_change_city/' + this.value,
                    success: function(data) {
                        location.reload();
                    }
                })
            });
        }
    }
})(jQuery);
