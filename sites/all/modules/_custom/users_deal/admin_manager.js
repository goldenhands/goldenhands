(function ($) {
    Drupal.behaviors.stat_chng = {
        attach:function (context, settings) {

            $('td > .form-type-select > .form-select').live('change', function () {

                var did = $(this).parent().parent().children('input[name$="did"]').val();
                var d_stat = $(this).val();

                $.ajax({
                    url:"/admin/config/users_deal_manage/status_change",
                    type:"POST",
                    data:{
                        'did':did,
                        'stat':d_stat
                    },
                    success:function () {
                        window.location.href = "/admin/config/users_deal/users_deal_manage"
                    }
                });
            });

            $('.delete_link').css('cursor', 'pointer');

        }
    };
})(jQuery);