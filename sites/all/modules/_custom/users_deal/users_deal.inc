<?php
/*
 * Форма создания сделки
 */
function users_deal_create_deal_form($form, &$form_state, $nid) {
  global $user;
  $advert = node_load($nid);
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['executor_uid'] = array(
    '#type' => 'hidden',
    '#value' => $advert->uid,
  );

  $form['customer_uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );

  $node = node_load($nid);
  $default_price_type = ec_advert_get_default_price_type($node->field_work_type['und'][0]['tid']);
  $path = drupal_get_path('theme', 'eventcust') . '/images/plus.png';
  $prices = ec_advert_get_prices($node->nid);

  //add prices values to js
  drupal_add_js(array('prices' => $prices), 'setting');

  //get localized price types in machine_name -> human_name array format
  $price_types = ec_advert_get_price_types($node->field_work_type['und'][0]['tid']);
  $form['price_types'] = array(
    '#type' => 'select',
    //'#title' => t('Choose price'),
    '#options' => $price_types,
    '#default_value' => $default_price_type,
  );

  $form['deal_part_start'] = array('#markup' => '<div id="deal-part">');

  $format = 'd.m.y';
  $form['date_start'] = array(
    '#type' => 'date_popup',
    '#default_value' => '',
    '#date_format' => $format,
    '#date_year_range' => '0:+1',
    '#date_label_position' => 'none',
    '#datepicker_options' => array('minDate' => 0, 'changeMonth' => FALSE, 'changeYear' => FALSE),
    '#prefix' => '<div class="create-deal-date-start"><div class="create-deal-date-label">' . t('Date') . ': </div>',
    '#suffix' => '</div>',
    '#size' => 7,
    '#required' => TRUE,
    '#attributes' => array('style' => 'float:left'),
    '#title' => t('Date start'),
    '#title_display' => 'invisible',
  );
  $form['date_end'] = array(
    '#type' => 'date_popup',
    '#default_value' => '',
    '#date_format' => $format,
    '#date_year_range' => '0:+1',
    '#date_label_position' => 'none',
    '#datepicker_options' => array('minDate' => 0, 'changeMonth' => FALSE, 'changeYear' => FALSE),
    '#prefix' => '<div class="create-deal-date-end"><div class="create-deal-date-label"> - </div>',
    '#suffix' => '</div>',
    '#required' => TRUE,
    '#size' => 7,
    '#title' => t('Date end'),
    '#title_display' => 'invisible',
  );

  //error message block (invalid dates)
  $form['error_msg'] = array('#markup' => '<div class="error-msg"></div>');

  $form['days_count'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );

  $form['price_val'] = array(
    '#type' => 'hidden',
    '#value' => 0,
  );

  $form['price_prefix'] = array(
    '#markup' => '<div class="deal-price-wrapper"><span class="advert-price">' . t('All Price: '),
  );

  $form['price'] = array(
    '#markup' => '',
    '#prefix' => '<span class="all-deal-price">',
    '#suffix' => '</span>',
  );

  $form['price_suffix'] = array(
    '#markup' => ' ' . t('rub.') . '</span></div>',
  );

  $form['submit'] = array(
    '#prefix' => '<div class="wrapper-order-button">' . theme('image', array('path' => $path)) . '',
    '#suffix' => '</div>',
    '#type' => 'submit',
    '#value' => t('Order', array(), array('context' => 'users_deal')),

  );

  $form['deal_part_end'] = array('#markup' => '</div>');

  return $form;
}

/**
 * Validation of a users_deal_create_deal_form().
 * @param $form
 * @param $form_state
 */
function users_deal_create_deal_form_validate($form, &$form_state) {
  //validates only on data submit
  if (empty($form_state['values']['date_start'])) {
    form_set_error('date_start', t('Choose a date'));
  }
  if ($form_state['values']['customer_uid'] == 0) {
    form_set_error('customer_uid', t('!login to create a deal at !link', array('!login' => l(t('Login'), 'user/login'))));
  }
  if ($form_state['values']['customer_uid'] == $form_state['values']['executor_uid']) {
    form_set_error('customer_uid', t('You can\'t create deal with yourself'));
  }
}

/*
 * Создание сделки
 */
function users_deal_create_deal_form_submit($form, &$form_state) {
  $date_start = strtotime($form_state['values']['date_start']);
  $date_end = strtotime($form_state['values']['date_end']);
  $error = false;
  global $user; // for customer params
  $user = user_load($user->uid);
  $nid = arg(1);
  $advert = node_load($nid); // for executor params
  $param['phone'] = ec_advert_order_setting_exist($advert->uid,'phone_number',true);
  $param['photo'] = ec_advert_order_setting_exist($advert->uid,'photo',true);

  if ($user->uid == 0) {
    $error = true;
  }

  if ($user->uid == $advert->uid) {
    drupal_set_message('You can not make deals with yourself.','error');
    $error = true;
  }

  if ($param['phone']['value']==1&&!_sms_custom_phone_confirmed($user)){
    drupal_set_message('You will have to verify their phone number before create deal with '.$advert->name,'error');
    $error = true;
  }
  //if path to photo = default picture then user hav not photo
  $filepath = ec_user_get_avatar_url($user->uid);

  if ($param['photo']['value']==1&&variable_get('user_picture_default', '')==$filepath){
    drupal_set_message('You will have to verify your photo before create deal with '.$advert->name,'error');
    $error = true;
  }

if (!$error){
  //  сохраняем сделку в бд
  $deal_id = db_insert('users_deal')
    ->fields(array(
    'date_start' => $date_start,
    'date_end' => $date_end,
    'customer_uid' => $form_state['values']['customer_uid'],
    'executor_uid' => $form_state['values']['executor_uid'],
    'advert_nid' => $form_state['values']['nid'],
    'days_count' => $form_state['input']['days_count'],
    'price' => $form_state['input']['price_val'],
    'price_type' => $form_state['values']['price_types'],
    'changed' => REQUEST_TIME,
    'status' => DEAL_STATUS_CREATED,
  ))
    ->execute();

  module_invoke_all('send_notice', $status = DEAL_STATUS_CREATED, $form_state['values']['customer_uid'], $form_state['values']['executor_uid'], $deal_id);

  $customer = user_load($form_state['values']['customer_uid']);
  $executor = user_load($form_state['values']['executor_uid']);

  drupal_set_message(t('Order added'));
  //Запись в журнал.
  $type = t('Users deal');
  $message = t('Created order by customer: %customer and executor: %executor', array(
    '%customer' => $customer->name,
    '%executor' => $executor->name
  ));
  _users_deal_log_enter($type, $message, $deal_id);
  if (module_exists('user_dialogs')) {
    //перенаправляем на страницу диалогов
    $thread_id = user_dialogs_get_thread_id($form_state['values']['customer_uid'], $form_state['values']['executor_uid']);
    $form_state['redirect'] = 'user/' . $form_state['values']['customer_uid'] . '/dialogs/threads/' . $thread_id;
  }
  else {
    //перенаправляем на страницу просмотра сделки
    $form_state['redirect'] = 'users_deal/deal/' . $deal_id;
  }
}
}

function users_deal_send_sms_mail($code) {
  global $user;


  if (module_exists('sms_custom')) {
    if (!(isset($user->sms_custom['status']) &&
      ($user->sms_custom['status'] == 0 ||
        $user->sms_custom['status'] == SMS_CUSTOM_PHONE_PENDING)
    )
    ) {
      $number = _sms_custom_get_user_number($user->uid);
      switch ($number->status) {
        case 0 :
          drupal_set_message(t('You must confirm your phone number'));
          drupal_goto('user/' . $GLOBALS['user']->uid . '/confirm_mobile');
          break;
        case 1 :
          drupal_set_message(t('You must confirm your phone number'));
          drupal_goto('user/' . $GLOBALS['user']->uid . '/confirm_mobile');
          break;
        case 3 :
          drupal_set_message(t('User not exists'));
          break;
        case 4 :
          drupal_set_message(t('You must add your phone number'));
          drupal_goto('user/' . $GLOBALS['user']->uid . '/settings');
          break;
        case 2 :
          //sms_send($number->number, _sms_custom_confirm_message($code));
          drupal_set_message(t('SMS with confirmation code sent to your phone'));
          return 1;
          break;
      }
    }
  }
  else {
    $to = $user->mail;
    $subject = t('Email verify');
    mail($to, $subject, t('Your confirmation code: ') . $code);
    drupal_set_message(t('Email with confirmation code sent to you'));
    if ($code) {
      watchdog('SMS', $code);
    }
  }
}

/*
 * SMS-code reset.
 */
function _users_deal_ret($form, &$form_state) {
  _users_deal_set_code(TRUE);
  $form_state['rebuild'] = TRUE;
  return;
}

/*
 * Set up code if not exists or reset it.
 */
function _users_deal_set_code($reset = FALSE) {
  if ($reset || (isset($_SESSION['users_deal_code_time']) && time() - $_SESSION['users_deal_code_time'] > 600)) {
    unset($_SESSION['users_deal_code']);
  }

  $ud = NULL;

  if (!@isset($_SESSION['users_deal_code'])) {
    $code = _user_deal_random_code();
//        dsm($code);
    $_SESSION['users_deal_code_time'] = time();
    $_SESSION['users_deal_code'] = md5($code);
    $ud = users_deal_send_sms_mail($code);


    //todo remove this after testing!
    drupal_set_message('Code is: ' . $code . '. REMOVE DEBUG CODE!');

    if ($code) {
      watchdog('SMS code', $code);
    }
  }
  //TODO FOR AMGRADE!!! Comment this line or remove it
  if (($ud == 1) /* || ($ud == null && users_deal_send_sms_mail($code) == 1)*/) {
    return 1;
  }
}

/*
 * Просмотр сделки
 */
function users_deal_view_deal($did) {
  global $user;


  $result = db_select('users_deal', 'ud')
    ->condition('deal_id', $did)
    ->fields('ud', array())
    ->execute();

  //why foreach here? Doesn't it only one object in any case?
  $output = '';
  //Add small calendar.

  foreach ($result as $row) {
    $executor = user_load($row->executor_uid);
    $output .= t('Executor') . ': ' . theme('username', array('account' => $executor)) . '<br>';
    $customer = user_load($row->customer_uid);
    $output .= t('Customer') . ': ' . theme('username', array('account' => $customer)) . '<br>';
    $output .= t('Period: from ') . date('d-m-Y', $row->date_start) . t(' to ') . date('d-m-Y', $row->date_end);

    $deal_price = $row->price;

    if ($row->discount) {
      $discount = $deal_price * (100 - $row->discount) / 100;
      $output .= '<h3>' . t('Discount') . ': ' . $discount . t('rub.') . '</h3>';
      $output .= '<h3>' . t('Price with discount') . ': ' . $deal_price - $discount . t('rub.') . '</h3>';
    }


    $output .= t('Last change') . ': ' . date('d-m-Y H:i', $row->changed) . '<br>';
    //todo change to function
    _users_deal_get_deal_actions($row);
    if ($row->status == DEAL_STATUS_CONFLICTED_BY_EXECUTOR) {
      $output .= t('Conflicted by executor');
    }
    if ($row->status == DEAL_STATUS_CONFLICTED_BY_CUSTOMER) {
      $output .= t('Conflicted by customer');
    }
    //On deal approved
    if ($row->status == DEAL_STATUS_APPROVED) {
      $output .= t('Waiting for payment') . '<br>';
      if ($user->uid == $customer->uid) {
        $output .= l(t('Pay out Deal'), 'users_deal/deal/' . $did . '/pay');
      }
    }

    if ($row->status == DEAL_STATUS_PAID_BY_CUSTOMER) {
      $output .= t('Status') . ': ' . t('Approved and paid') . '<br>';

      $output .= t('Actions') . ': ' . l(t(' Conflict '), 'users_deal/deal/' . $did . '/conflict') . ' ';

    }


    if (in_array($row->status,
      array(
        DEAL_STATUS_PAID_BY_CUSTOMER,
        DEAL_STATUS_PAYOUT
      ))
    ) {
      $output .= t('All tasks completed');
      if ($row->status == DEAL_STATUS_PAID_BY_CUSTOMER) {
        $output .= ' ' . l(t(' Complete '), 'users_deal/' . $did . '/completed');
      }
      if ($row->executor_uid == $user->uid && $row->status == DEAL_STATUS_PAYOUT) {
        $output .= ' ' . l(t(' Complete '), 'users_deal/' . $did . '/completed');
      }
      if ($row->status == DEAL_STATUS_PAYOUT) {
        $output .= ' <strong>  Finished </strong> ';
      }
      else {
        $output .= ' ' . l(t(' Conflict '), 'users_deal/deal/' . $did . '/conflict') . ' ';
      }
    }
    else {
      if (in_array($row->status,
        array(
          DEAL_STATUS_CREATED,
          DEAL_STATUS_EDITED,
          DEAL_STATUS_APPROVED
        ))
      ) {
        $output .= l(t('Change parameters'), 'users_deal/deal/' . $did . '/edit');
        if (($row->executor_uid == $user->uid && $row->status != DEAL_STATUS_APPROVED)
          || ($row->customer_uid == $user->uid && $row->status != DEAL_STATUS_APPROVED)
        ) {
          $output .= ' | ' . l(t('Approve'), 'users_deal/deal/' . $did . '/approve');
        }
        $output .= ' | ' . l(t('Delete'), 'users_deal/deal/' . $did . '/delete');
      }
    }
  }

  return $output;
}

function _users_deal_get_deal_actions($deal) {
  /*todo
    move status output to another function
    change global user to executor object - need for admin
  */
  global $user;
  $customer = user_load($deal->customer_uid);
  $output = '';
  $actions = array();
  if ($deal->status == DEAL_STATUS_CONFLICTED_BY_EXECUTOR) {
    $output .= t('Conflicted by executor');
  }
  if ($deal->status == DEAL_STATUS_CONFLICTED_BY_CUSTOMER) {
    $output .= t('Conflicted by customer');
  }
  //On deal approved
  if ($deal->status == DEAL_STATUS_APPROVED) {
    $output .= t('Waiting for payment') . '<br>';
    if ($user->uid == $customer->uid) {
      $actions[] = array('text' => t('Pay out Deal'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/pay');
    }
  }

  if ($deal->status == DEAL_STATUS_PAID_BY_CUSTOMER) {
    $output .= t('Status') . ': ' . t('Approved and paid') . '<br>';
    $actions[] = array('text' => t('Conflict'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/conflict');
  }
  if (in_array($deal->status,
    array(
      DEAL_STATUS_PAID_BY_CUSTOMER,
      DEAL_STATUS_PAYOUT
    ))
  ) {
    $output .= t('All tasks completed');
    if ($deal->status == DEAL_STATUS_PAID_BY_CUSTOMER) {
      $actions[] = array('text' => t('Complete'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/completed');
    }
    if ($deal->executor_uid == $user->uid && $deal->status == DEAL_STATUS_PAYOUT) {
      $actions[] = array('text' => t('Complete'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/completed');
    }
    if ($deal->status == DEAL_STATUS_PAYOUT) {
      $output .= ' <strong>  Finished </strong> ';
    }
    else {
      $actions[] = array('text' => t('Conflict'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/conflict');
    }
  }
  else {
    if (in_array($deal->status,
      array(
        DEAL_STATUS_CREATED,
        DEAL_STATUS_EDITED,
        DEAL_STATUS_APPROVED
      ))
    ) {
      $actions[] = array('text' => t('Change parameters'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/edit');
      if (($deal->executor_uid == $user->uid && $deal->status != DEAL_STATUS_APPROVED)
        || ($deal->customer_uid == $user->uid && $deal->status != DEAL_STATUS_APPROVED)
      ) {
        $actions[] = array('text' => t('Approve'), 'link' => 'users_deal/' . $deal->deal_id . 'approve/');
      }
      $actions[] = array('text' => t('Delete'), 'link' => 'users_deal/deal/' . $deal->deal_id . '/delete');
    }
  }
  //dsm($actions);
}

/**
 * Функция для подтверждение заказа
 *
 * @param $did
 */
function users_deal_approve_form($form, &$form_state, $did) {
  if (count($form_state['input']) == 0) {
    _users_deal_set_code(TRUE);
  }
  $form['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Code here'),
    '#preffix' => t('You have received SMS, enter your code here')
  );

  $form['did'] = array(
    '#type' => 'hidden',
    '#value' => $did,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Ok'),
    '#submit' => array('users_deal_approve_form_submit')
  );

  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Send code again'),
    '#submit' => array('_users_deal_ret'),
    '#prefix' => '<h6>' . t('If you have not received the code, please click "Send code again"') . '</h6>'
  );

  return $form;
}

function users_deal_approve_form_validate($form, &$form_state) {
  if (md5($form_state['values']['code']) !== $_SESSION['users_deal_code']) {
    form_set_error('code', t('Wrong code'));
  }
}

/**
 * Проверка смс-а для подтверждение заказа
 *
 * @param $form
 * @param $form_state
 * @return void
 */
function users_deal_approve_form_submit($form, &$form_state) {
  $did = isset($form_state['values']['did']) ? $form_state['values']['did'] : '';

  if ($did != '') {
    global $user;
    $request = db_select('users_deal', 'ud')
      ->fields('ud', array('executor_uid', 'customer_uid', 'status'))
      ->condition('deal_id', $did)
      ->execute()
      ->fetchAssoc();

    $new_status = NULL;
    switch ($user->uid) {
      case $request['executor_uid']:
        $new_status =
          $request['status'] == DEAL_STATUS_APPROVED_BY_CUSTOMER ?
            DEAL_STATUS_APPROVED : DEAL_STATUS_APPROVED_BY_EXECUTOR;
        _users_deal_log_enter(
          'Approve',
          'Deal approved by executor', $did
        );
        module_invoke_all('send_notice', $request['status'], $request['customer_uid'], $request['executor_uid'], $did);
        break;
      case $request['customer_uid']:
        $new_status =
          $request['status'] == DEAL_STATUS_APPROVED_BY_EXECUTOR ?
            DEAL_STATUS_APPROVED : DEAL_STATUS_APPROVED_BY_CUSTOMER;

        _users_deal_log_enter(
          'Approve',
          'Deal approved by customer ', $did
        );
        module_invoke_all('send_notice', $request['status'], $request['customer_uid'], $request['executor_uid'], $did);
        break;
      default:
        drupal_set_message(t('Error on deal approve.'), 'error');
        return;
    }

    db_update('users_deal')
      ->fields(array('status' => $new_status))
      ->condition('deal_id', $did)
      ->condition('status', $request['status']) //this isn't a guaranty the status hasn't change, but preserve from simple mistakes with approve from edited state
      ->execute();
    unset($_SESSION['users_deal_code']);
    drupal_goto('users_deal/deal/' . $did);
  }
  else {
    drupal_set_message(t('Unknown deal.'), 'error');
  }
}

/*
 * Deal delete confirmation
 */
function _users_deal_delete_form($form, $form_state, $did) {
  return confirm_form(
    array(
      'did' => array(
        '#type' => 'value',
        '#value' => $did,
      ),
    ),
    t('Are you sure you want to remove deal %did?', array(
      '%did' => $did,
    )),
    'admin/config/users_deal/users_deal_manage',
    t('This action cannot be undone.'),
    t('Delete deal'),
    t('Cancel')
  );
}

/**
 * Delete deal
 * @param $did
 * @return void
 */
function _users_deal_delete_form_submit($form, $form_state) {
  global $user;
  $did = $form_state['values']['did'];
  $deal_name = t('Deal №') . $did;
  $result = db_delete('users_deal')
    ->condition('deal_id', $did)
    ->execute();
  if ($result) {
    module_invoke_all('pre_delete', $did, $user->name);
    drupal_set_message(t('@deal_name deleted', array('@deal_name' => $deal_name)));
    //Запись в журнал
    $type = t('Delete');
    $message = t('Deal N:%did deleted by: %user', array('%did' => $did, '%user' => $user->name));
    _users_deal_log_enter($type, $message, $did);
    //drupal_goto('adverts'); WHY??????????????
    drupal_goto('admin/config/users_deal/users_deal_manage');
  }
}

/**
 * функция для переноса сделки в статус конфликта
 *
 * @param $did
 * @return void
 */
function _users_deal_conflict($did) {
  global $user;

  $result = db_select('users_deal', 'ud')
    ->fields('ud')
    ->condition('deal_id', $did)
    ->execute();

  foreach ($result as $row) {
    $customer = $row->customer_uid;
    $executor = $row->executor_uid;
  }

  switch ($user->uid) {
    case $customer :
      $status = DEAL_STATUS_CONFLICTED_BY_CUSTOMER;
      break;
    case $executor :
      $status = DEAL_STATUS_CONFLICTED_BY_EXECUTOR;
      break;
  }

  db_update('users_deal')
    ->fields(array(
    'status' => $status,
  ))
    ->condition('deal_id', $did)
    ->execute();

  //Отправить оповещание
  module_invoke_all('send_notice', $status, $customer, $executor, $did);

  //Запись в журнал
  $type = users_deal_get_status($status);
  $message = t('Deal N:%did conflicted by: %user', array('%did' => $did, '%user' => $user->name));
  _users_deal_log_enter($type['text'], $message, $did);

  drupal_goto('users_deal/deal/' . $did);
}

/**
 * Генерация случайного кода.
 *
 * @return string
 */
function _user_deal_random_code() {
  return sprintf("%06d", rand(0, 999999));
}

/**
 * Настройка величины скидки
 *
 * @return array
 */
function _users_deal_setting_form() {

  $form = array();

  $form['discount'] = array(
    '#type' => 'textfield',
    '#title' => t('Users Deal Discount'),
    '#description' => t('"Discount values separated by |" '),
    '#default_value' => array(variable_get('discount')),
  );

  return system_settings_form($form);
}


/**
 * Админская часть сделок : показ всех сделок
 *
 * @return string
 */
function _users_deal_manager() {
  global $user;

  $mod_path = drupal_get_path('module', 'users_deal');
  drupal_add_js($mod_path . '/admin_manager.js');

  $output = '';
  $output .= "<h2>" . t('Hello manager') . ' "' . $user->name . '"</h2>';

  $db = db_select('users_deal', 'ud');

  if (isset($_GET['status'])) {
    $db->condition('status', $_GET['status']);
  }

  if (isset ($_GET['date-from']) && isset($_GET['date-to'])) {
    $from = $_GET['date-from'];
    $to = $_GET['date-to'];

    $from_stamp = (int) mktime(0, 0, 0, $from['month'], $from['day'], $from['year']);
    $to_stamp = (int) mktime(0, 0, 0, $to['month'], $to['day'], $to['year']);

    if ($from_stamp > $to_stamp) {
      drupal_set_message(t('You select wrong date'), 'error');
    }
    else {
      $db->condition('date_created', $from_stamp, '>=');
      $db->condition('date_created', $to_stamp, '<=');
    }
  }

  $db->fields('ud');

  $result = $db->execute();

  $variables = array();

  $status = users_deal_status_options('deals');

  $variables['header'] = array(
    t('Deal number'),
    t('Deal'),
    t('Executor'),
    t('Customer'),
    t('Status'),
    t('Start date'),
    t('End date'),
    t('Edit status'),
    t('Action')
  );

  $i = 0;

  foreach ($result as $row) {

    $form['stat_edit'] = array(
      'stat_edit' . $row->deal_id => array(
        '#type' => 'select',
        '#title' => '',
        '#options' => $status,
        '#value' => $row->status,
      )
    );

    $form['did'] = array(
      '#type' => 'hidden',
      '#value' => $row->deal_id,
      '#name' => 'did',
    );

    $variables['rows'][] = array(
      ++$i,
      l(t('Deal №') . $row->deal_id, 'users_deal/deal/' . $row->deal_id),
      user_load($row->customer_uid)->name,
      user_load($row->executor_uid)->name,
      $status[$row->status],
      date('d/M/Y', $row->date_start),
      date('d/M/Y', $row->date_end),
      drupal_render($form['stat_edit']) . drupal_render($form['did']),
      //'delete' => l(t('Delete'), 'users_deal/deal/' . $row->deal_id . '/delete'),
      l(t('Delete'), 'users_deal/deal/' . $row->deal_id . '/delete')
    );
  }

  $form_filter = drupal_get_form('_users_deal_manager_filter');
  $output .= drupal_render($form_filter);
  $output .= theme('table', $variables);


  return $output;
}


/**
 * Фильтр админской части сделок
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function _users_deal_manager_filter($form, &$form_state) {
  $form = array();

  $status = isset($_GET['status']) ? users_deal_get_status($_GET['status']) : "";

//  print_r ($status['text']); exit;
  $options = users_deal_status_options('deals');

  $form['#method'] = 'get';

  $form['status'] = array(
    '#type' => 'select',
    '#prefix' => '<h2>' . t('Filter') . '</h2>',
    '#title' => t('Status'),
    '#options' => $options,
    '#default_value' => isset($_GET['status']) ? $_GET['status'] : DEAL_STATUS_CREATED,
  );

  $form['date-from'] = array(
    '#type' => 'date',
    '#title' => t('From'),
    '#default_value' => isset ($_GET['date-from']) ? $_GET['date-from'] : "",
  );

  $form['date-to'] = array(
    '#type' => 'date',
    '#title' => t('To'),
    '#default_value' => isset ($_GET['date-to']) ? $_GET['date-to'] : "",
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Filter',
  );

  return $form;
}

/**
 * Записать лог в базу
 *
 * @param $type
 * @param $message
 * @param null $did
 * @return void
 */
function _users_deal_log_enter($type, $message, $did) {
  db_insert('deal_logs')
    ->fields(array(
    'deal_id' => $did,
    'type' => $type,
    'message' => $message,
    'created_on' => REQUEST_TIME,
  ))
    ->execute();
}

function _users_deal_ret_filter_status() {
  $type = array(
    1 => 'None',
    2 => 'Users deal',
    3 => 'Deal edit',
    4 => 'Discount change',
    5 => 'Complete',
    6 => 'Delete'
  );

  return $type;
}


/**
 * Админская часть показа логов
 *
 * @return string
 */
function _users_deal_log_view() {
  $output = '';

  $db = db_select('deal_logs', 'dl');

  if (isset ($_GET['date-from']) && isset($_GET['date-to'])) {
    $from = $_GET['date-from'];
    $to = $_GET['date-to'];

    $from_stamp = (int) mktime(0, 0, 0, $from['month'], $from['day'], $from['year']);
    $to_stamp = (int) mktime(0, 0, 0, $to['month'], $to['day'], $to['year']);

    if ($from_stamp > $to_stamp) {
      drupal_set_message(t('You select wrong date'), 'error');
    }
    else {
      $db->condition('created_on', $from_stamp, '>=');
      $db->condition('created_on', $to_stamp, '<=');
    }
  }

  $type = _users_deal_ret_filter_status();

  if (isset ($_GET['type']) && $type[$_GET['type']] != "None") {
    $db->condition('type', $type[$_GET['type']]);
  }

  if (isset ($_GET['deal_id']) && $_GET['deal_id'] != '') {
    $db->condition('deal_id', $_GET['deal_id']);
  }


  $db->fields('dl');
  $db->orderBy('created_on', 'DESC');
  $result = $db->execute();

  $variables = array();

  $variables['header'] = array(
    t('No'),
    t('Deal'),
    t('Type'),
    t('Message'),
    t('Date')
  );

  foreach ($result as $row) {
    $variables['rows'][] = array(
      'log_number' => $row->id,
      'links' => l(t('Deal №') . $row->deal_id, 'users_deal/deal/' . $row->deal_id),
      'type' => $row->type,
      'message' => $row->message,
      'date' => date('d/M/Y', $row->created_on),
    );
  }

  if (!empty($variables)) {
    $output .= drupal_render(drupal_get_form('_users_deal_logs_filter'));
  }
  $output .= theme('table', $variables);

  return $output;
}

function _users_deal_logs_filter($form, &$form_state) {
  $form = array();

  $form['#method'] = 'get';

  $type = _users_deal_ret_filter_status();

  $def_type = isset ($_GET['type']) ? $_GET['type'] : "";

  $form['deal_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Deal ID')
  );

  $form['date-from'] = array(
    '#type' => 'date',
    '#title' => t('From'),
    '#default_value' => isset ($_GET['date-from']) ? $_GET['date-from'] : "",
  );

  $form['date-to'] = array(
    '#type' => 'date',
    '#title' => t('To'),
    '#default_value' => isset ($_GET['date-to']) ? $_GET['date-to'] : "",
  );

  $form['type'] = array(
    '#type' => 'select',
    '#options' => $type,
    '#title' => t('Type'),
    '#default_value' => $def_type
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Filter',
  );

  return $form;
}

/**
 * Dummie for pay
 *
 * @param $form
 * @param $form_state
 * @param $deal_id
 * @return array
 */
function users_deal_deal_pay_form($form, $form_state, $deal_id) {
  $res = db_select('users_deal', 'ud')
    ->fields('ud', array())
    ->condition('deal_id', $deal_id)
    ->execute();

  foreach ($res as $deal) {
    if ($deal->days_count > 0) {
      $amount = $deal->days_count * $deal->price;
    }
    else {
      $amount = $deal->price;
    }
    if ($deal->discount > 0) {
      $amount = round($amount * (100 - $deal->discount) / 100, 2);
    }
  }

  $form['deal_id'] = array(
    '#type' => 'hidden',
    '#value' => $deal_id,
  );

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#value' => $amount,
    '#disabled' => TRUE,
  );

  $form['pay_method'] = array(
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#options' => array(1 => 'Bank card'),
  );


  $form['pay'] = array(
    '#type' => 'submit',
    '#value' => t('Pay deal'),
  );
  return $form;

}

/**
 * Submit deal payment
 *
 * @param $form
 * @param $form_state
 * @return void
 */
function users_deal_deal_pay_form_submit($form, &$form_state) {
  //if payment by credit card
  if ($form_state['values']['pay_method'] == 1) {
    //todo Проверить. может ли другой пользователь оплатить, например админ
    global $user;
    $deal_id = $form_state['values']['deal_id'];
    $link = 'users_deal/deal/' . $deal_id . '/check_payment';
    $order_id = db_insert('user_orders')
      ->fields(array(
      'external_id' => NULL,
      'amount' => (int) $form_state['values']['amount'],
      'status' => 1, //created
      'deal_id' => $deal_id,
      'uid' => $user->uid,
      'payment_system' => (int) $form_state['values']['pay_method']
    ))
      ->execute();
    watchdog(t('Payment'), t('Created order %order_id', array('%order_id' => $order_id)));
    if ($order_id !== FALSE) {
      $params = array(
        'returnUrl' => url($link, array('absolute' => TRUE)),
        'amount' => $form_state['values']['amount'] * 100,
        'orderNumber' => $order_id,
        'description' => t('Payment deal number @deal_id', array('@deal_id' => $deal_id)),
      );
      $result = _alfa_payment_rest_function('registerPreAuth', $params);
      if (is_array($result) && isset($result['errorCode'])) {
        if ($result['errorCode'] == '-1') {
          drupal_set_message(t('Service unavailable. Try later.'));
        }
        else {
          drupal_set_message(t('Error on order create. Payment system return error: "%errorMessage"', array('%errorMessage' => $result['errorMessage'])));
        }
      }
      else {
        user_deal_pay_deal($deal_id);
        $deal = users_deal_get_deal($deal_id);
        $advert = node_load($deal->advert_nid);
        $text = t('Deal of !advert_link paid by customer.', array('!advert_link' => l($advert->title, 'node/' . $advert->nid)));
        user_dialogs_message_send($deal->customer_uid, $deal->executor_uid, $text, USER_DIALOGS_SYSTEM_PAY_DEAL, NULL, $deal_id);

        $external_id = $result->orderId;
        db_update('user_orders')
          ->fields(array('external_id' => $external_id))
          ->condition('internal_id', $order_id)
          ->execute();
        $formUrl = $result->formUrl;
        drupal_goto($formUrl, array('absolute' => TRUE));
      }
    }
    else {
      drupal_set_message(t('Error on order create.'));
      watchdog(t('Payment'), t('Error on create order in db. User deal %deal, amount %amount'), array(
        '%deal' => $deal_id,
        '%amount' => $form_state['values']['amount']
      ));
    }
  }
}

function _users_deal_check_deal_payment($deal_id) {
  if (isset($_GET['orderId'])) {
    $order_id = $_GET['orderId'];
    $order = db_select('user_orders', 'uo')
      ->fields('uo', array('amount', 'internal_id'))
      ->condition('external_id', $order_id)
      ->execute()
      ->fetchAssoc();
    $params = array(
      'amount' => $order['amount'] * 100,
      'orderId' => $order_id,
    );
    $result = _alfa_payment_rest_function('deposit', $params);
    if (is_array($result) && isset($result['errorCode'])) {
      if ($result['errorCode'] == '-1') {
        drupal_set_message(t('Service unavailable. Try later.'));
      }
      else {
        drupal_set_message(t('Error on order create. Payment system return error: "%errorMessage"', array('%errorMessage' => $result['errorMessage'])));
      }
      _alfa_payment_delete_order($order->internal_id, $result);
    }
    else {
      drupal_set_message(t('Your order successfully paid.'));
      db_update('users_deal')
        ->condition('deal_id', $deal_id)
        ->fields(array(
        'status' => DEAL_STATUS_PAID_BY_CUSTOMER,
      ))
        ->execute();
      db_update('user_orders')
        ->fields(array('external_id' => $order_id))
        ->condition('status', 4) //deposited
        ->execute();

      global $user;
      module_invoke_all('add_user_money_info', $user->uid, 1, 1, $order['amount'], 1, $deal_id, 'Payment of %deal_id');
      _users_deal_log_enter('Pay', 'Deal payed', $deal_id);
    }
  }
  else {
    drupal_set_message(t('Error on payment.'));
    watchdog(t('Payment'), t('Error on payment. External order id not found for deal %deal'), array('%deal' => $deal_id));
  }
  drupal_goto('users_deal/deal/' . $deal_id);
}

/**
 * Implements hook_send_notice
 *
 * @param $status
 * @param $customer
 * @param $executor
 * @param $did
 * @return void
 */
function users_deal_send_notice($status, $customer, $executor, $did) {
  global $user;

  $deal_name = t('Deal №') . $did;
  $body = "";
  $author = $user->uid;
  $recipient = ($author == $customer) ? $executor : $customer;
  $deal = users_deal_get_deal($did);
  $deal_advert = node_load($deal->advert_nid);
  $deal_advert_link = l($deal_advert->title, 'node/' . $deal_advert->nid, array('html' => TRUE));

  switch ($status) {
    case DEAL_STATUS_PAYOUT:
      $body = t('@deal_name closed', array('@deal_name' => $deal_name));
      $user_dialog_type = USER_DIALOGS_SIMPLE;
      break;
    case DEAL_STATUS_PAID_BY_CUSTOMER:
      $body = t('@deal_name paid by customer', array('@deal_name' => $deal_name));
      $user_dialog_type = USER_DIALOGS_SYSTEM_CONFLICT_DEAL;
      break;
    case DEAL_STATUS_APPROVED:
      $body = t("@deal_name approved", array('@deal_name' => $deal_name));
      $user_dialog_type = USER_DIALOGS_SYSTEM_PAY_DEAL;
      break;
    case DEAL_STATUS_CONFLICTED_BY_EXECUTOR:
      $body = t("@deal_name conflicted by executor", array('@deal_name' => $deal_name));
      $user_dialog_type = USER_DIALOGS_SYSTEM_SOLVE;
      break;
    case DEAL_STATUS_CONFLICTED_BY_CUSTOMER:
      $body = t("@deal_name conflicted by customer", array('@deal_name' => $deal_name));
      $user_dialog_type = USER_DIALOGS_SYSTEM_SOLVE;
      break;
    case DEAL_STATUS_CREATED:
      $body = t("Request for !deal_advert_link", array('!deal_advert_link' => $deal_advert_link));
      $user_dialog_type = USER_DIALOGS_SYSTEM_REQUEST_DEAL;
      break;
    case DEAL_STATUS_CANCELED:
      $body = t("Request for !deal_advert_link declined", array('!deal_advert_link' => $deal_advert_link));
      $user_dialog_type = USER_DIALOGS_SYSTEM_DECLINE_DEAL;
      break;
  }

  if ($body != "") {
    if (module_exists('user_dialogs')) {
      user_dialogs_message_send($author, $recipient, $body, $user_dialog_type, NULL, $did);
    }
  }
}

function user_deal_request_dialogs_form($form, $form_state, $message) {
  $form = array();
  drupal_add_css(drupal_get_path('module', 'user_dialogs') . '/user_dialogs.css');

  $deal = users_deal_get_deal($message->did);
  $deal_advert = node_load($deal->advert_nid);
  $deal_info = t('Request by advert !advert_link', array('!advert_link' => l($deal_advert->title, 'node/' . $deal_advert->nid))) . '<br>';
  $deal_info .= t('from !date_start to !date_end', array(
    '!date_start' => date('d M Y', $deal->date_start),
    '!date_end' => date('d M Y', $deal->date_end)
  )) . '<br>';
  $deal_price_type = ec_advert_load_price_type($deal->price_type);
  $deal_info .= t('Need: !price_type.', array('!price_type' => $deal_price_type->name)) . '<br>';
  $deal_info .= t('Price: !price RUR', array('!price' => $deal->price)) . '<br>';


  $form['deal_info'] = array(
    '#markup' => $deal_info,
  );

  $form['action_list'] = array(
    '#type' => 'radios',
    '#options' => array('accept' => t('Accept'), 'decline' => t('Decline'), 'need_info' => t('Request info')),
    '#title' => t('Select Action'),
  );

  $form['reason'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Reason'),
  );

  $form['text'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Text'),
  );

  $form['did'] = array(
    '#type' => 'hidden',
    '#value' => $message->did,
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'users_deal') . '/users_deal.form.js',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

function user_deal_request_dialogs_form_submit($form, $form_state) {
  global $user;
  $did = $form_state['values']['did'];
  $deal = users_deal_get_deal($did);
  $action = $form_state['values']['action_list'];
  switch ($action) {
    case 'accept':
      //approve deal
      user_deal_approve_deal($did);
      $advert = node_load($deal->advert_nid);
      $text = t('Request for !advert_link approved by executor.', array('!advert_link' => l($advert->title, 'node/' . $advert->nid)));
      user_dialogs_message_send($deal->executor_uid, $deal->customer_uid, $text, USER_DIALOGS_SYSTEM_APPROVE_DEAL, NULL, $did);
      break;
    case 'decline':
      //decline deal
      user_deal_cancel_deal($did);
      $advert = node_load($deal->advert_nid);
      $comment_text = ($form_state['values']['reason'] != '') ? $form_state['values']['reason'] : t('Unknown');
      $reason_text = t('Request for !advert_link declined by executor. <br> Reason of decline: !text', array(
        '!advert_link' => l($advert->title, 'node/' . $advert->nid),
        '!text' => $comment_text
      ));
      user_dialogs_message_send($deal->executor_uid, $deal->customer_uid, $reason_text, USER_DIALOGS_SYSTEM_DECLINE_DEAL, NULL, $did);
      break;
    case 'need_info':
      //request info
      user_dialogs_message_send($deal->executor_uid, $deal->customer_uid, $form_state['values']['text'], USER_DIALOGS_SIMPLE, NULL, $did);
      break;
  }
}

/*
 * Cancel user deal request
 */
function user_deal_cancel_deal($did) {
  db_update('users_deal')
    ->fields(array('status' => DEAL_STATUS_CANCELED))
    ->condition('deal_id', $did)
    ->execute();
}

/*
 * Approve user deal request
 */
function user_deal_approve_deal($did) {
  db_update('users_deal')
    ->fields(array('status' => DEAL_STATUS_APPROVED))
    ->condition('deal_id', $did)
    ->execute();
}

/*
 * Pay user deal request
 */
function user_deal_pay_deal($did) {
  db_update('users_deal')
    ->fields(array('status' => DEAL_STATUS_PAID_BY_CUSTOMER))
    ->condition('deal_id', $did)
    ->execute();
}