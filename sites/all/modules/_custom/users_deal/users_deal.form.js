(function ($) {
    Drupal.behaviors.request_deal_form = {
        attach:function (context, settings) {
            function hideDeclineText() {
                $('#user-deal-request-dialogs-form .form-item-action-list .form-item:has(#edit-action-list-decline) label:not(.option)').hide();
                $('#user-deal-request-dialogs-form .form-item-action-list .form-textarea-wrapper:has(textarea#edit-reason)').hide();
            };
            function showDeclineText() {
                $('#user-deal-request-dialogs-form .form-item-action-list .form-item:has(#edit-action-list-decline) label:not(.option)').show();
                $('#user-deal-request-dialogs-form .form-item-action-list .form-textarea-wrapper:has(textarea#edit-reason)').show();
            };

            function hideMessageText() {
                $('#user-deal-request-dialogs-form .form-item-action-list .form-item:has(#edit-action-list-need-info) label:not(.option)').hide();
                $('#user-deal-request-dialogs-form .form-item-action-list .form-textarea-wrapper:has(textarea#edit-text)').hide();
            };
            function showMessageText() {
                $('#user-deal-request-dialogs-form .form-item-action-list .form-item:has(#edit-action-list-need-info) label:not(.option)').show();
                $('#user-deal-request-dialogs-form .form-item-action-list .form-textarea-wrapper:has(textarea#edit-text)').show();
            };

            //add reason text to decline option and remove it
            $('#user-deal-request-dialogs-form .form-type-radios .form-radios .form-item-action-list:has(input#edit-action-list-decline) label').
                after($('#user-deal-request-dialogs-form .form-item-reason').html());

            $('#user-deal-request-dialogs-form .form-item-reason').html('');
            hideDeclineText();

            //add message text to need info option and remove it
            $('#user-deal-request-dialogs-form .form-type-radios .form-radios .form-item-action-list:has(input#edit-action-list-need-info) label').
                after($('#user-deal-request-dialogs-form .form-item-text').html());
            $('#user-deal-request-dialogs-form .form-item-text').html('');
            hideMessageText();

            $('#user-deal-request-dialogs-form .form-type-radios .form-radios .form-item-action-list input#edit-action-list-decline').live('click', function () {
                showDeclineText();
                hideMessageText();
            });

            $('#user-deal-request-dialogs-form .form-type-radios .form-radios .form-item-action-list input#edit-action-list-need-info').live('click', function () {
                showMessageText();
                hideDeclineText();
            });
            $('#user-deal-request-dialogs-form .form-type-radios .form-radios .form-item-action-list input#edit-action-list-accept').live('click', function () {
                hideMessageText();
                hideDeclineText();
            });
        }
    };


})(jQuery);