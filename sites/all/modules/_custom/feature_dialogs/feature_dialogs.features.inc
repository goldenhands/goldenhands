<?php
/**
 * @file
 * feature_dialogs.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function feature_dialogs_image_default_styles() {
  $styles = array();

  // Exported image style: user_dialog_avatar
  $styles['user_dialog_avatar'] = array(
    'name' => 'user_dialog_avatar',
    'effects' => array(
      1 => array(
        'label' => 'Масштабирование',
        'help' => 'Масштабирование позволяет изменить размеры изображения с сохранением пропорций. Если введён размер только одной стороны, то размер другой будет вычислен автоматически. Если введены два размера, то каждое будет определять максимальный размер по своему направлению и применяться в зависимости от формата изображения.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '50',
          'height' => '50',
          'upscale' => 0,
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}
