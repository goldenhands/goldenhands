/**
 * Created with JetBrains PhpStorm.
 * User: MrFeathers
 * Date: 10.01.13
 * Time: 19:30
 * To change this template use File | Settings | File Templates.
 */

(function ($) {
    Drupal.behaviors.hybridauth_custom = {
        attach: function(context, settings) {
            $(document).ready(function(){

                $('#fb-invates-form .form-type-checkbox').each(function(){
                           id=$(this).children('input').val();
                           photo=Drupal.settings.hybridauth_custom['img'][id];
                           name = Drupal.settings.hybridauth_custom['names'][id];
                           $(this).css('backgroundImage','url('+photo+')');
                           $(this).wrap('<div class="fb-friends-wrapper"/>');
                           $(this).parent('.fb-friends-wrapper').append('<span class="fb-friends-name">'+name+'<span/>');

                    });
            });

            $('.fb-friends-wrapper').click(function(){
                changeCheck($(this));
            })
        }
    };

    function changeCheck(el)
    {
        var el = el,
        input = el.find("input").eq(0);

        if(!input.attr("checked")) {
            input.attr("checked", true)
            el.addClass('fb-friends-checked');
        } else {
            input.attr("checked", false)
            el.removeClass('fb-friends-checked');
        }

    }

})(jQuery);