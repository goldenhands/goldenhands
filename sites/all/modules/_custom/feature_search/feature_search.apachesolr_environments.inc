<?php
/**
 * @file
 * feature_search.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function feature_search_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'Solr server';
  $environment->url = 'http://localhost:8983/solr/eventcust_test';
  $environment->service_class = '';
  $environment->conf = array(
    'apachesolr_read_only' => '0',
    'apachesolr_search_facet_pages' => '',
  );
  $environment->index_bundles = array(
    'node' => array(
      0 => 'advert',
    ),
    'user' => array(
      0 => 'user',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
