﻿(function ($) {
    Drupal.behaviors.ecadvert = {
        attach:function (context, settings) {
            // show\hide author another adverts
            $('a#another_author_adverts_browse').live('click', function () {
                var cl = document.getElementById('another_author_adverts_browse');
                block = $('#block-ec-advert-author-another-adverts .content-roll');
                if (block.is(":hidden")) {
                    cl.innerHTML = Drupal.t('view less');
                    block.slideDown('slow');
                }
                else
                {
                    cl.innerHTML = Drupal.t('view more');
                    block.slideUp('slow');
                }
                return false;
            })
            // show\hide Related adverts
            $('a#Related_adverts_browse').live('click', function () {
                var cl = document.getElementById('Related_adverts_browse');
                block = $('#block-ec-advert-related-adverts .content-roll');
                if (block.is(":hidden")) {
                    cl.innerHTML = Drupal.t('view less');
                    block.slideDown('slow');
                }
                else
                {
                    cl.innerHTML = Drupal.t('view more');
                    block.slideUp('slow');
                }
                return false;
            })
        }
    }

    Drupal.behaviors.ecadvert_prices = {
        attach:function (context, settings) {
            //initialize form elements
            $('#advert-node-form .work-price-category').parent().hide();
            var tid = $('#advert-node-form #edit-field-work-type-und').find(":selected").val();
            if (tid != '_none'){
                $('#advert_work_price_container .fieldset-description').hide();
                $('#advert-node-form .work-price-category').parent().hide();
                $('#advert-node-form .work-price-category-' + tid).parent().slideDown();
            }


            $('#advert-node-form #edit-field-work-type-und').change(function(){
                var tid = $(this).find(":selected").val();
                if (tid != '_none'){
                    $('#advert_work_price_container .fieldset-description').hide();
                    $('#advert-node-form .work-price-category').parent().hide();
                    $('#advert-node-form .work-price-category-' + tid).parent().slideDown();
                }
                else {
                    $('#advert_work_price_container .fieldset-description').show();
                }
            });
        }
    }
}(jQuery));


