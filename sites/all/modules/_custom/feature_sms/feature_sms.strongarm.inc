<?php
/**
 * @file
 * feature_sms.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_sms_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_custom_confirmation_message';
  $strongarm->value = '[site:name] confirmation code:[sms-user:confirm-code]';
  $export['sms_custom_confirmation_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_custom_max_chars';
  $strongarm->value = '140';
  $export['sms_custom_max_chars'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_custom_registration_form';
  $strongarm->value = '0';
  $export['sms_custom_registration_form'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_custom_sleep';
  $strongarm->value = 1;
  $export['sms_custom_sleep'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_default_gateway';
  $strongarm->value = 'prostor_gatewaygateway';
  $export['sms_default_gateway'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_notify_text_pm';
  $strongarm->value = '!message_text';
  $export['sms_notify_text_pm'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sms_prostor_gatewaygateway_settings';
  $strongarm->value = array(
    'sms_prostor_gateway_base_url' => 'gate.prostor-sms.ru/send/',
    'sms_prostor_gateway_method' => 'GET',
    'sms_prostor_gateway_user_field' => 'login',
    'sms_prostor_gateway_user_value' => 'vdGoldenHands',
    'sms_prostor_gateway_pass_field' => 'password',
    'sms_prostor_gateway_pass_value' => '662538',
    'sms_prostor_gateway_sender_field' => 'EventCust',
    'sms_prostor_gateway_number_field' => 'phone',
    'sms_prostor_gateway_message_field' => 'text',
    'form_build_id' => 'form-l_4SSgKxEL1KREONKFzR0pHNjnlBKhOhrk7CZU619JQ',
  );
  $export['sms_prostor_gatewaygateway_settings'] = $strongarm;

  return $export;
}
