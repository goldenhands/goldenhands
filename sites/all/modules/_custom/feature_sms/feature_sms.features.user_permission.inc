<?php
/**
 * @file
 * feature_sms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_sms_user_default_permissions() {
  $permissions = array();

  // Exported permission: edit own sms number.
  $permissions['edit own sms number'] = array(
    'name' => 'edit own sms number',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'manager',
    ),
    'module' => 'sms_custom',
  );

  // Exported permission: receive sms.
  $permissions['receive sms'] = array(
    'name' => 'receive sms',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'manager',
    ),
    'module' => 'sms_custom',
  );

  return $permissions;
}
