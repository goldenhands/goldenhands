<?php
/**
 * @file
 * feature_frontpage.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_frontpage_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function feature_frontpage_image_default_styles() {
  $styles = array();

  // Exported image style: frontpage_slideshow_img_style.
  $styles['frontpage_slideshow_img_style'] = array(
    'name' => 'frontpage_slideshow_img_style',
    'effects' => array(
      3 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '1600',
          'height' => '600',
        ),
        'weight' => '2',
      ),
    ),
  );

  return $styles;
}
