<?php
/**
 * @file
 * feature_tenders.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function feature_tenders_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'tenders';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'tenders';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tenders';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_work_type' => 'field_work_type',
    'created' => 'created',
    'field_work_period' => 'field_work_period',
    'comment_count' => 'comment_count',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_work_type' => array(
      'align' => '',
      'separator' => ',',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_work_period' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'comment_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Tender name';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Work type */
  $handler->display->display_options['fields']['field_work_type']['id'] = 'field_work_type';
  $handler->display->display_options['fields']['field_work_type']['table'] = 'field_data_field_work_type';
  $handler->display->display_options['fields']['field_work_type']['field'] = 'field_work_type';
  $handler->display->display_options['fields']['field_work_type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_work_type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_work_type']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_work_type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_work_type']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_work_type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_work_type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_work_type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_work_type']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_work_type']['type'] = 'i18n_taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_work_type']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_work_type']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_work_type']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_work_type']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_work_type']['field_api_classes'] = 0;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['created']['date_format'] = 'only_date';
  /* Field: Content: Work Period */
  $handler->display->display_options['fields']['field_work_period']['id'] = 'field_work_period';
  $handler->display->display_options['fields']['field_work_period']['table'] = 'field_data_field_work_period';
  $handler->display->display_options['fields']['field_work_period']['field'] = 'field_work_period';
  $handler->display->display_options['fields']['field_work_period']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_work_period']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_work_period']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_work_period']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_work_period']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_work_period']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_work_period']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_work_period']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_work_period']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_work_period']['settings'] = array(
    'format_type' => 'only_date',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_work_period']['field_api_classes'] = 0;
  /* Field: Content: Comment count */
  $handler->display->display_options['fields']['comment_count']['id'] = 'comment_count';
  $handler->display->display_options['fields']['comment_count']['table'] = 'node_comment_statistics';
  $handler->display->display_options['fields']['comment_count']['field'] = 'comment_count';
  $handler->display->display_options['fields']['comment_count']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['external'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['comment_count']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['comment_count']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['comment_count']['alter']['html'] = 0;
  $handler->display->display_options['fields']['comment_count']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['comment_count']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['comment_count']['hide_empty'] = 0;
  $handler->display->display_options['fields']['comment_count']['empty_zero'] = 0;
  $handler->display->display_options['fields']['comment_count']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['comment_count']['separator'] = '';
  $handler->display->display_options['fields']['comment_count']['format_plural'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tenders' => 'tenders',
  );
  /* Filter criterion: Content: Work Period -  start date (field_work_period) */
  $handler->display->display_options['filters']['field_work_period_value']['id'] = 'field_work_period_value';
  $handler->display->display_options['filters']['field_work_period_value']['table'] = 'field_data_field_work_period';
  $handler->display->display_options['filters']['field_work_period_value']['field'] = 'field_work_period_value';
  $handler->display->display_options['filters']['field_work_period_value']['operator'] = 'between';
  $handler->display->display_options['filters']['field_work_period_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_work_period_value']['expose']['operator_id'] = 'field_work_period_value_op';
  $handler->display->display_options['filters']['field_work_period_value']['expose']['label'] = 'Work Period';
  $handler->display->display_options['filters']['field_work_period_value']['expose']['operator'] = 'field_work_period_value_op';
  $handler->display->display_options['filters']['field_work_period_value']['expose']['identifier'] = 'field_work_period_value';
  $handler->display->display_options['filters']['field_work_period_value']['expose']['multiple'] = FALSE;
  $handler->display->display_options['filters']['field_work_period_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_work_period_value']['default_to_date'] = 'now +1 month';
  /* Filter criterion: Content: Work type (field_work_type:delta) (translated) */
  $handler->display->display_options['filters']['delta_i18n']['id'] = 'delta_i18n';
  $handler->display->display_options['filters']['delta_i18n']['table'] = 'field_data_field_work_type';
  $handler->display->display_options['filters']['delta_i18n']['field'] = 'delta_i18n';
  $handler->display->display_options['filters']['delta_i18n']['exposed'] = TRUE;
  $handler->display->display_options['filters']['delta_i18n']['expose']['operator_id'] = 'delta_i18n_op';
  $handler->display->display_options['filters']['delta_i18n']['expose']['label'] = 'Work type';
  $handler->display->display_options['filters']['delta_i18n']['expose']['operator'] = 'delta_i18n_op';
  $handler->display->display_options['filters']['delta_i18n']['expose']['identifier'] = 'delta_i18n';
  $handler->display->display_options['filters']['delta_i18n']['expose']['multiple'] = 1;
  $handler->display->display_options['filters']['delta_i18n']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['delta_i18n']['reduce_duplicates'] = 1;
  $handler->display->display_options['filters']['delta_i18n']['type'] = 'select';
  $handler->display->display_options['filters']['delta_i18n']['vocabulary'] = 'work_category';
  $handler->display->display_options['filters']['delta_i18n']['hierarchy'] = 1;
  $handler->display->display_options['filters']['delta_i18n']['error_message'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'tenders';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Tenders';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['tenders'] = $view;

  return $export;
}
