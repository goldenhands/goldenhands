<?php
/**
 * @file
 * feature_adverts.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_adverts_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advert_city|node|advert|form';
  $field_group->group_name = 'group_advert_city';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advert';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_advert_fields';
  $field_group->data = array(
    'label' => 'City',
    'weight' => '4',
    'children' => array(
      0 => 'field_city',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_advert_city|node|advert|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advert_description|node|advert|form';
  $field_group->group_name = 'group_advert_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advert';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_advert_fields';
  $field_group->data = array(
    'label' => 'Description',
    'weight' => '2',
    'children' => array(
      0 => 'field_short_description',
      1 => 'field_description',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_advert_description|node|advert|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advert_fields|node|advert|form';
  $field_group->group_name = 'group_advert_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advert';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Advert fields',
    'weight' => '0',
    'children' => array(
      0 => 'group_common',
      1 => 'group_advert_description',
      2 => 'group_advert_photo',
      3 => 'group_advert_city',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_advert_fields|node|advert|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advert_photo|node|advert|form';
  $field_group->group_name = 'group_advert_photo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advert';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_advert_fields';
  $field_group->data = array(
    'label' => 'Photo',
    'weight' => '3',
    'children' => array(
      0 => 'field_ads_fotos',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_advert_photo|node|advert|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_common|node|advert|form';
  $field_group->group_name = 'group_common';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'advert';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_advert_fields';
  $field_group->data = array(
    'label' => 'Common',
    'weight' => '1',
    'children' => array(
      0 => 'field_work_price',
      1 => 'field_work_type',
      2 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_common|node|advert|form'] = $field_group;

  return $export;
}
