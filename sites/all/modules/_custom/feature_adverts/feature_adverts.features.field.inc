<?php
/**
 * @file
 * feature_adverts.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function feature_adverts_field_default_fields() {
  $fields = array();

  // Exported field: 'node-advert-field_ads_fotos'.
  $fields['node-advert-field_ads_fotos'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_ads_fotos',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'advert',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'galleryformatter',
          'settings' => array(
            'link_to_full' => 1,
            'link_to_full_style' => '0',
            'linking_method' => 'onclick_full',
            'modal' => 'colorbox',
            'slide_style' => 'advert-slider-big-image',
            'style' => 'Greenarrows',
            'thumb_style' => 'advert-slider-images',
          ),
          'type' => 'galleryformatter_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_ads_fotos',
      'label' => 'Image',
      'required' => 0,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'multiupload_imagefield_widget',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_miw',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-advert-field_city'.
  $fields['node-advert-field_city'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_city',
      'foreign keys' => array(),
      'indexes' => array(
        'glid' => array(
          0 => 'glid',
        ),
      ),
      'locked' => '0',
      'module' => 'getlocations_fields',
      'settings' => array(
        'autocomplete_bias' => 0,
        'baselayers' => array(
          'Hybrid' => 1,
          'Map' => 1,
          'Physical' => 1,
          'Satellite' => 1,
        ),
        'behavior' => array(
          'overview' => 0,
          'overview_opened' => 0,
          'scale' => 0,
          'scrollwheel' => 0,
        ),
        'city_autocomplete' => '1',
        'comment_map_marker' => 'drupal',
        'controltype' => 'small',
        'country' => 'RU',
        'draggable' => 1,
        'input_additional_required' => '4',
        'input_additional_weight' => '0',
        'input_additional_width' => '40',
        'input_address_width' => '40',
        'input_city_required' => '0',
        'input_city_weight' => '0',
        'input_city_width' => '40',
        'input_country_required' => '0',
        'input_country_weight' => '0',
        'input_country_width' => '40',
        'input_geobutton_weight' => '0',
        'input_geolocation_button_weight' => '0',
        'input_latitude_weight' => '0',
        'input_latitude_width' => '20',
        'input_longitude_weight' => '0',
        'input_longitude_width' => '20',
        'input_map_marker' => 'drupal',
        'input_map_weight' => '0',
        'input_marker_weight' => '0',
        'input_name_required' => '4',
        'input_name_weight' => '0',
        'input_name_width' => '40',
        'input_postal_code_required' => '4',
        'input_postal_code_weight' => '0',
        'input_postal_code_width' => '40',
        'input_province_required' => '0',
        'input_province_weight' => '0',
        'input_province_width' => '40',
        'input_smart_ip_button_weight' => '0',
        'input_street_required' => '4',
        'input_street_weight' => '0',
        'input_street_width' => '40',
        'latlon_warning' => 1,
        'latlong' => '40,0',
        'map_backgroundcolor' => '',
        'map_marker' => 'drupal',
        'mapheight' => '200px',
        'maptype' => 'Map',
        'mapwidth' => '300px',
        'mtc' => 'standard',
        'node_map_marker' => 'drupal',
        'only_continents' => '',
        'only_countries' => '',
        'pancontrol' => 1,
        'per_item_marker' => 0,
        'province_autocomplete' => '1',
        'restrict_by_country' => 1,
        'search_country' => 'RU',
        'use_address' => '2',
        'use_country_dropdown' => '1',
        'use_geolocation_button' => 0,
        'use_smart_ip_button' => 1,
        'use_smart_ip_latlon' => 0,
        'user_map_marker' => 'drupal',
        'vocabulary_map_marker' => 'drupal',
        'zoom' => '3',
      ),
      'translatable' => '0',
      'type' => 'getlocations_fields',
    ),
    'field_instance' => array(
      'bundle' => 'advert',
      'default_value' => array(
        0 => array(
          'address' => '- - -',
          'name' => '',
          'street' => '',
          'additional' => '',
          'city' => '-',
          'province' => '-',
          'postal_code' => '',
          'country' => 'RU',
          'marker' => '',
          'latitude' => '',
          'longitude' => '',
          'active' => FALSE,
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '5',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_city',
      'label' => 'City',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'getlocations_fields',
        'settings' => array(
          'city_autocomplete' => '0',
          'country' => 'RU',
          'input_additional_required' => '0',
          'input_additional_weight' => '0',
          'input_additional_width' => '40',
          'input_address_width' => '40',
          'input_city_required' => '0',
          'input_city_weight' => '0',
          'input_city_width' => '40',
          'input_country_required' => '0',
          'input_country_weight' => '0',
          'input_country_width' => '40',
          'input_latitude_weight' => '0',
          'input_latitude_width' => '20',
          'input_longitude_weight' => '0',
          'input_longitude_width' => '20',
          'input_name_required' => '0',
          'input_name_weight' => '0',
          'input_name_width' => '40',
          'input_postal_code_required' => '0',
          'input_postal_code_weight' => '0',
          'input_postal_code_width' => '40',
          'input_province_required' => '0',
          'input_province_weight' => '0',
          'input_province_width' => '40',
          'input_street_required' => '0',
          'input_street_weight' => '0',
          'input_street_width' => '40',
          'location_taxonomize' => 0,
          'per_item_marker' => 0,
          'province_autocomplete' => '0',
          'use_address' => '1',
          'use_country_dropdown' => '1',
        ),
        'type' => 'getlocations_fields',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-advert-field_description'.
  $fields['node-advert-field_description'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_description',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'advert',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '6',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_description',
      'label' => 'Description',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-advert-field_short_description'.
  $fields['node-advert-field_short_description'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_short_description',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'advert',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_short_description',
      'label' => 'Short Description',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-advert-field_work_type'.
  $fields['node-advert-field_work_type'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_work_type',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'locked' => '0',
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'work_category',
            'parent' => '0',
          ),
        ),
        'options_list_callback' => 'i18n_taxonomy_allowed_values',
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'advert',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_work_type',
      'label' => 'Work type',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'taxonomy_term-work_category-field_category_foto'.
  $fields['taxonomy_term-work_category-field_category_foto'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_category_foto',
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '0',
      'type' => 'image',
    ),
    'field_instance' => array(
      'bundle' => 'work_category',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'image',
          'settings' => array(
            'image_link' => '',
            'image_style' => '',
          ),
          'type' => 'image',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'taxonomy_term',
      'field_name' => 'field_category_foto',
      'label' => 'Category Foto',
      'required' => 1,
      'settings' => array(
        'alt_field' => 0,
        'default_image' => 0,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '32',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Category Foto');
  t('City');
  t('Description');
  t('Image');
  t('Short Description');
  t('Work type');

  return $fields;
}
