<?php
/**
 * @file
 * feature_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Exported menu: menu-footer-first.
  $menus['menu-footer-first'] = array(
    'menu_name' => 'menu-footer-first',
    'title' => 'Footer first',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-footer-second.
  $menus['menu-footer-second'] = array(
    'menu_name' => 'menu-footer-second',
    'title' => 'Footer second',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-footer-third.
  $menus['menu-footer-third'] = array(
    'menu_name' => 'menu-footer-third',
    'title' => 'Footer third',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-header-menu.
  $menus['menu-header-menu'] = array(
    'menu_name' => 'menu-header-menu',
    'title' => 'Header menu',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-header-register-menu.
  $menus['menu-header-register-menu'] = array(
    'menu_name' => 'menu-header-register-menu',
    'title' => 'Header register menu',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-header-user-menu.
  $menus['menu-header-user-menu'] = array(
    'menu_name' => 'menu-header-user-menu',
    'title' => 'Header user menu',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-help.
  $menus['menu-help'] = array(
    'menu_name' => 'menu-help',
    'title' => 'Help',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-help-english-.
  $menus['menu-help-english-'] = array(
    'menu_name' => 'menu-help-english-',
    'title' => 'Help(english)',
    'description' => '',
    'language' => 'en',
    'i18n_mode' => '2',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer first');
  t('Footer second');
  t('Footer third');
  t('Header menu');
  t('Header register menu');
  t('Header user menu');
  t('Help');
  t('Help(english)');
  t('Main menu');
  t('Management');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>Management</em> menu contains links for administrative tasks.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');


  return $menus;
}
