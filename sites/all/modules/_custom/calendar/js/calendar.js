/**
 * Behaviors for calendar.
 */

(function ($) {
  // Render FullCalendar.
  Drupal.behaviors.handsCalendar = {
    attach:function (context, settings) {
      var pathToCalendar = window.location.pathname.match(/[0-9]+/);
      var user = pathToCalendar != null ? parseInt(pathToCalendar) : 'me';

      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();

      if($('#calendar').length){
        var calendar = $('#calendar', context).fullCalendar({
          header:{
            left:'prev,next today',
            center:'title',
            right:''
          },
          events:{
            url:'/calendar-get-ajax-data',
            data:{'user':user}
          },
          firstDay:1,

          dayClick:function (date, allDay, jsEvent, view) {
            var td = $(this);
            var stateCheckBox = false;
            var state = td.hasClass('day-off');
            var timestamp = Math.floor(date.getTime() / 1000);
            stateCheckBox = state ? 'checked' : '';
            td.removeData('qtip').qtip({
              id: 'modal',
              content:{text:'<input data-timestamp = "' + timestamp + '" data-parent = "' + td.attr('class') + '" type="checkbox" name="checkbox" class="check-day-off" ' + stateCheckBox + '/><label class="option">' + Drupal.t('Make it a rest day?') + '</label>',
                title:{
                  text:' ',
                  button:true
                }
              },
              position:{
                my:'center',
                at:'center',
                target:td
              },
              show:{
                solo:true,
                event:false,
                ready:true,
                modal:true
              },
              hide:false,
              style:'ui-tooltip-light ui-tooltip-rounded'
            });
          },
          eventRender:function (event, element, view) {
            $('.datepickerWeek, .datepickerDoW > th:first').hide().closest('.datepickerContainer').find('.datepickerMonth').attr('colspan', 5);
            var id = event.id;
            var color = event.color;
            if(event.class_name == 'event-hidden'){
              element.addClass(event.class_name);
            }
            $('#deals-list .deal-color[data-deal="' + id + '"]').css({
              'background-color': color
            });
            element.qtip({
              id:'modal',
              content:{
                text:event.deal_info,
                title:{
                  text:event.title,
                  button:true
                }
              },
              position:{
                my:'center',
                at:'center',
                target:element,
                adjust:{
                  mouse:true
                }
              },
              style:'ui-tooltip-light ui-tooltip-rounded',
              show:{
                event:'click',
                solo:true,
                modal:true
              },
              hide:'unfocus'
            });

            var days = event.days_off;
            setDayOff(days, calendar);

            $('.fc-button-prev span', context).live('click', function () {
              setDayOff(days, calendar);
            });
            $('.fc-button-next span', context).live('click', function () {
              setDayOff(days, calendar);
            });
          },

          eventResize:function (event, dayDelta, minuteDelta, revertFunc) {
            var startTimestamp = parseInt(event.start.getTime() / 1000);
            var endTimestamp = event.end ? parseInt(event.end.getTime() / 1000) : '';
            var idEvent = event.id;

            $.get('/calendar-ajax-update', {
              'startTimestamp':startTimestamp,
              'endTimestamp':endTimestamp,
              'idEvent':idEvent
            }, function () {
              $('#calendar').fullCalendar('refetchEvents');
            });
            return false;
          },
          eventDrop:function (event, dayDelta, minuteDelta, allDay, revertFunc) {

            var startTimestamp = parseInt(event.start.getTime() / 1000);
            var endTimestamp = event.end ? parseInt(event.end.getTime() / 1000) : '';
            var idEvent = event.id;
            $.get('/calendar-ajax-update', {
              'startTimestamp':startTimestamp,
              'endTimestamp':endTimestamp,
              'idEvent':idEvent
            }, function () {
              $('#calendar').fullCalendar('refetchEvents');
            });
            return false;
          }
        });
      }
      $('.check-day-off', context).live('change', function () {
        var self = $(this);
        var parent = self.data('Parent');
        var timestamp = self.data('Timestamp');
        var state = self.attr('checked');
        if (state === true) {
          if ($('#calendar table tbody td').hasClass(parent)) {
            var className = '.' + parent.split(' ')[2];
            $.get('/calendar-ajax-day-off', {'dayTimestamp':timestamp, 'status':state, 'user':user}, function () {
              $(className).addClass('day-off');
            });
          }
        }
        else {
          if ($('#calendar table tbody td').hasClass(parent)) {
            var className = '.' + parent.split(' ')[2];
            $.get('/calendar-ajax-day-off', {'dayTimestamp':timestamp, 'status':state, 'user':user}, function () {
              $(className).removeClass('day-off');
            });
          }
        }
      });

      //Weekends
      //Set busy
      $('.weekend-wrapper a#weekend-busy', context).click(function (e) {
        e.preventDefault();
        var status = 'busy';
        var weekends = $.merge($('tbody .fc-sat'), $('tbody .fc-sun'));
        var otherMonth = $('tbody .fc-week0 .fc-other-month').length - 1;
        var weekendDays = [];
        $.each(weekends, function (i, n) {
          if (!$(this).hasClass('fc-other-month')) {
            var daysWeekend = $(this).find('.fc-day-number').html();
            var monthWeekend = $(calendar).fullCalendar('getDate').getMonth() + 1;
            var yearWeekend = $(calendar).fullCalendar('getDate').getFullYear();
            var dateWeekend = Number(daysWeekend) + '-' + monthWeekend + '-' + yearWeekend;
            weekendDays.push(dateWeekend);
          }
        });
        $.get('/calendar-ajax-weekend', {'weekendDays':weekendDays, 'weekendStatus':status, 'user':user}, function (data) {
          var weekend = data;
          var monthWeekend = $(calendar).fullCalendar('getDate').getMonth() + 1;
          $.each(weekend, function (i, n) {
            var arrayData = n.split(':');
            if (arrayData[2] == 'day-off') {
              if (arrayData[1] == monthWeekend) {
                $('.fc-day' + (Number(arrayData[0]) + Number(otherMonth))).addClass('day-off');
              }
            }
          });
        });
      });
      //Set free
      $('.weekend-wrapper a#weekend-free', context).click(function (e) {
        e.preventDefault();
        var status = 'free';
        var weekends = $.merge($('tbody .fc-sat'), $('tbody .fc-sun'));
        var otherMonth = $('tbody .fc-week0 .fc-other-month').length - 1;
        var weekendDays = [];
        $.each(weekends, function (i, n) {
          if (!$(this).hasClass('fc-other-month')) {
            var daysWeekend = $(this).find('.fc-day-number').html();
            var monthWeekend = $(calendar).fullCalendar('getDate').getMonth() + 1;
            var yearWeekend = $(calendar).fullCalendar('getDate').getFullYear();
            var dateWeekend = Number(daysWeekend) + '-' + monthWeekend + '-' + yearWeekend;
            weekendDays.push(dateWeekend);
          }
        });
        $.get('/calendar-ajax-weekend', {'weekendDays':weekendDays, 'weekendStatus':status, 'user':user}, function (data) {
          var weekend = data;
          var monthWeekend = $(calendar).fullCalendar('getDate').getMonth() + 1;
          $.each(weekend, function (i, n) {
            var arrayData = n.split(':');
            if (arrayData[2] != 'day-off') {
              if (arrayData[1] == monthWeekend) {
                $('.fc-day' + (Number(arrayData[0]) + Number(otherMonth))).removeClass('day-off');
              }
            }
          });
        });
      });
    }
  };

  // Accordion on the first sidebar.
  Drupal.behaviors.accordionDeals = {
    attach:function (context, settings) {
      if ($('#deals-list, #completed-deals-list').length) {
        $('#deals-list, #completed-deals-list').accordion({
          collapsible:true,
          autoHeight:false,
          active:false
        });

        $('#deals-list .ui-accordion-li-fix .item-list, #completed-deals-list .ui-accordion-li-fix .item-list').each(function(){
          var path = '';
          if($('#deals-list').length) {
            path = $(this).parent().find('.ui-accordion-content .progress-bar');
            $(this).insertAfter(path);
          }
          if($('#completed-deals-list').length){
            path = $(this).parent().find('.ui-accordion-content .deal-price');
            $(this).insertBefore(path);
          }
        });
      }
    }
  };

  // Change status day.
  Drupal.behaviors.checkedWorks = {
    attach:function (context, settings) {
      $('.work-item', context).change(function () {
        var self = $(this);
        if (self.attr('checked') == true) {
          $.get('/ajax-works-checkbox', {workTid:self.attr('tid'), dealId:self.attr('deal-id')}, function (data) {
            self.qtip({
              id:'modal',
              content:{
                text:data,
                title:{
                  text:' ',
                  button:true
                }
              },
              position:{
                my:'center',
                at:'center',
                target:self,
                adjust:{
                  mouse:true
                }
              },
              style:'ui-tooltip-light ui-tooltip-rounded',
              show:{
                solo:true,
                event:false,
                ready:true,
                modal:true
              },
              hide:'unfocus'
            });
          });
        }
      });

      $('#-users-deal-task-pay-form #pay_button').live('click', function (e) {
        e.preventDefault();
        var self = $(this);
        var inputCode = self.parent().find('.form-item-code input').val();
        var workId = self.parents('.ui-tooltip-content').children('.errors-list').attr('tid');
        var dealId = self.parents('.ui-tooltip-content').children('.errors-list').attr('deal_id');
        $.get('/ajax-code-validate', {code:inputCode, workId:workId, dealId:dealId}, function (data) {
          var data = data;
          if (data[0] != '') {
            self.parents('.ui-tooltip-content').children('.errors-list').html(data[0]);
          }
        });
      });
      $('#-users-deal-task-pay-form #edit-button').live('click', function (e) {
        e.preventDefault();
        $.get('/ajax-send-code-again', {}, function () {
        });
      });
    }
  };

  /**
   * Add day-off to calendar.
   * @param days
   * @param calendar
   */
  function setDayOff(days, calendar) {
    $('tbody .fc-widget-content').removeClass('day-off');
    var month = $(calendar).fullCalendar('getDate').getMonth();
    var otherMonth = $('tbody .fc-week0 .fc-other-month').length - 1;
    $.each(days, function (i, n) {
      var dayMonth = n.split(':');
      if (dayMonth[1] == month) {
        $('.fc-day' + (Number(dayMonth[0]) + Number(otherMonth))).addClass('day-off');
      }
    });
  }

  // Address of the user on the map.
  Drupal.behaviors.addressMap = {
    attach: function(context, settings){
      if($('.user-address a').length){
        $('.user-address a').click(function(e){
          e.preventDefault();
          var longitude = $(this).data('Longitude');
          var latitude = $(this).data('Latitude');
          $(this).qtip({
            id:'modal',
            content:{
              text:'<img src="http://maps.googleapis.com/maps/api/staticmap?center=' + latitude +',' + longitude + '&zoom=12&size=600x400&markers=color:blue%7Clabel:S%7C' + latitude +',' + longitude + '&sensor=false">',
              title:{
                text:' ',
                button:true
              }
            },
            position:{
              my:'center',
              at:'center',
              target:self,
              adjust:{
                mouse:true
              }
            },
            style:'ui-tooltip-light ui-tooltip-rounded',
            show:{
              solo:true,
              event:false,
              ready:true,
              modal:true
            },
            hide:'unfocus'
          });
        })
      }
    }
  };

  // Render small calendar.
  Drupal.behaviors.smallCalendar = {
    attach: function(context, settings){
      var startDay = new Date();
      var endDay = new Date();
      var dates = [];
      var dealId = '';
      var uid = '';
      var editDeal = '';

        /*
      // User uid.
      if(location.href.split('/')[3] == 'user'){
        uid = location.href.split('/')[4];
      }
      if(location.href.split('/')[3] == 'ru' && location.href.split('/')[4] == 'user'){
        uid = location.href.split('/')[5];
      }
      // Create deal.
      if(location.href.split('/')[4] == 'create'){
        dealId = location.href.split('/')[5];
      }
      if(location.href.split('/')[3] == 'ru' && location.href.split('/')[5] == 'create'){
        dealId = location.href.split('/')[6];
      }
      // Edit deal.
      if(location.href.split('/')[3] == 'users_deal' && location.href.split('/')[6] == 'edit'){
        editDeal = location.href.split('/')[5] + '/edit';
      }
      if(location.href.split('/')[3] == 'ru' && location.href.split('/')[4] == 'users_deal' && location.href.split('/')[7] == 'edit'){
        editDeal = location.href.split('/')[6] + '/edit';
      }*/
        // for adverts calendar
       if (Drupal.settings.user_id) uid = Drupal.settings.user_id;

      // Small calendar on user and big calendar page.
      if($('#small-calendar', context).length){
      //  $.get('/rest-days-for-small-calendar', {'uid': uid, 'dealId': dealId}, function(data){
          $.get('/rest-days-for-small-calendar/'+uid, function(data){
          if(!$.isEmptyObject(data)){
            dates = data['days'];
          }
          else{
            dates = '1971-01-01';
          }

          $('#small-calendar').datepicker({
            flat: true,
            date: dates,
            minDate: 0,
            firstDay: 1,
            format: 'Y-m-d',
            calendars: 1,
            mode: 'multiple',
            onRender: function(date) {
                	return {
            			disabled: (date.valueOf())
            		}
            	},
            starts: 1,
              //look every date in small calendar for make days_off
              //marking only if  days_off oldest then now date
            beforeShowDay: function(date) {
                // todo REWRITE!!!!!!
                var date_format = date.getFullYear()+'-';
                if (date.getMonth()+1<10) date_format += '0' +(date.getMonth()+1)+'-';
                else date_format += (date.getMonth()+1)+'-';
                if (date.getDate()<10) date_format +='0'+date.getDate();
                else date_format +=date.getDate();

                var now = new Date();
                var now_date_format = now.getFullYear()+'-'
                if (now.getMonth()+1<10) now_date_format += '0'+(now.getMonth()+1)+'-';
                else now_date_format += (now.getMonth()+1)+'-';
                if (now.getDate()<10) now_date_format +='0'+now.getDate();
                else now_date_format +=now.getDate();

                if (now_date_format <= date_format ){
                    if ($.inArray(date_format,Drupal.settings.days_off) >=0)
                    {
                        return [false,"day-off"];
                    }
                }

                return [true, ""];//enable all other days
            }
          });


          $('.datepickerDoW').closest('.datepickerContainer').find('.datepickerMonth').attr('colspan', 5);
          $('.datepickerGoPrev a, .datepickerGoNext a', context).live('click', function(e){
            $('.datepickerDoW').closest('.datepickerContainer').find('.datepickerMonth').attr('colspan', 5);
          });
        });
      };

      // Small calendar on create and edit deal page.
      if($('#deal-small-calendar', context).length){
       // $.get('/rest-days-for-small-calendar', {'uid': uid, 'dealId': dealId, 'editDeal': editDeal}, function(data){
          $.get('/rest-days-for-small-calendar/'+uid, function(data){
          if(data['status'] == 'edit'){
            dates = data['days'];
            startDay = new Date(dates[0]);
            endDay = new Date(dates[1]);
            if(startDay != null){
              var startValue = Math.floor(startDay / 1000) + 82800;
              var endValue = Math.floor(endDay / 1000) - 3599;
              $('#deal-start-date').val(startValue);
              $('#deal-end-date').val(endValue);
            }
          }
          else{
            dates = $.merge(['1971-01-01', '1971-01-01'], data['days']);
          }
          $('#deal-small-calendar', context).DatePicker({
            flat: true,
            date: dates,
            format: 'Y-m-d',
            calendars: 1,
            mode: 'range',
            onChange: function(formated, dates){
              startDay = dates[0];
              endDay = dates[1];
              $.each(dates, function(i, n){
                if(i > 1){
                  if(n.toString() == startDay){
                    var calendarId = $('#deal-small-calendar').data('datepickerId');
                    $('#deal-small-calendar #' + calendarId).data('datepicker').lastSel = false;
                    startDay = '';
                    endDay = '';
                    alert(Drupal.t('This day is busy. Please choose another one.'));
                  }
                }
              });
              if(startDay != null){
                var startValue = Math.floor(startDay.getTime() / 1000) + 82800;
                var endValue = Math.floor(endDay.getTime() / 1000) - 3599;
                $('#deal-start-date').val(startValue);
                $('#deal-end-date').val(endValue);
              }
            },
            starts: 1
          });
          $('.datepickerDoW').closest('.datepickerContainer').find('.datepickerMonth').attr('colspan', 5);
          $('.datepickerGoPrev a, .datepickerGoNext a', context).live('click', function(e){
            $('.datepickerDoW').closest('.datepickerContainer').find('.datepickerMonth').attr('colspan', 5);
          });
        });
      }
    }
  };
})(jQuery);