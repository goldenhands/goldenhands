<?php

function _calendar_list_view() {
  global $user;

  $output = "";

  $query = db_select('users_deal' , 'ud')
      ->fields('ud')
      ->condition(db_or()->condition('executor_uid' , $user->uid)->condition('customer_uid' , $user->uid))
      ->execute();
  $result = $query->fetchAll();

  if (!empty($result)) {
    $variables = array();

    foreach ($result as $deal){
      $variables[$deal->deal_id] = array(
        'name' => $deal->deal_id,
        'start_time' => $deal->date_start,
        'end_time' => $deal->date_end,
      );

    }
  }
  if (isset($variables))
    $output .= theme('list_cal',array('var'=>$variables));
  else {
    $output .= t('You do not have active deals');
  }
  return $output;
}

/**
 * impliment hook_theme
 * @return array
 */
function calendar_theme() {
  return array(
    'list_cal' => array(
      'variables' => array('var'=>NULL
      )
    )
  );
}

/**
 * Define list_cal theme
 * @param $variables
 * @return html output
 */
function theme_list_cal($var) {
  $output = "";
    $variables=$var['var'];
  if (!empty ($variables)) {
    $output .= '<table style="border:  1px solid black;">';
    $output .= '<tr>';
    $output .= '<th> ' . t('Deal name') . '</th>';
    $output .= '<th>' . t('Date') . '</th>';
    $output .= '<th>' . t('Price') . '</th>';
    $output .= '<th>' . t('Status') . '</th>';
    $output .= '</tr>';
    foreach ($variables as $deal) {

      $date = date('d.m.y', $deal['start_time']) . " - " . date('d.m.y', $deal['end_time']);

      $output .= '<tr>';
      $output .= '<td> ' . $deal['name'] . '</td>';
      $output .= '<td>' . $date . '</td>';
      $output .= '<td>' . " " . '</td>';
      $output .= '<td>' . " " . '</td>';
      $output .= '</tr>';
    }
    $output .= '</table>';
  } else {
    $output .= t('You do not have active deals');
  }

  return $output;
}